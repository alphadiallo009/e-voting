/*
 * (c) Copyright 2020 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.ComputedVerificationCardExistsException;
import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.ComputedVerificationCardNotFoundException;

@Service
@Repository
public class ComputedVerificationCardsRepository {

	private static final String FAILED_ACCESS_COMPUTED_VERIFICATION = "Failed trying to access computed verification cards table.";
	private static final String NO_EXISTING_COMPUTED_VERIFICATION_CARD_ENTRY_WITH = "No existing computed verification card entry with ";
	private static final String TRUE = "Y";
	private static final String FALSE = "N";

	@Autowired
	private DataSource dataSource;

	public void saveComputedVerificationCard(final ComputedVerificationCard computedVerificationCard) throws ComputedVerificationCardExistsException {
		String sql = "insert into computed_verification_cards (election_event_id, verification_card_id, confirmation_attempts, exponentiation_computed) values (?,?,?,?) ";

		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, computedVerificationCard.getElectionEventId());
			statement.setString(2, computedVerificationCard.getVerificationCardId());
			statement.setInt(3, computedVerificationCard.getConfirmationAttempts());
			statement.setString(4, computedVerificationCard.isExponentiationComputed() ? TRUE : FALSE);
			statement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new ComputedVerificationCardExistsException(
					"Existing computed verification card entry with " + verificationCardIdAndElectionEventIdStr(
							computedVerificationCard.getVerificationCardId(), computedVerificationCard.getElectionEventId()),
					e);
		} catch (SQLException e) {
			throw new IllegalStateException(FAILED_ACCESS_COMPUTED_VERIFICATION, e);
		}
	}

	public Optional<ComputedVerificationCard> findByElectionEventIdAndVerificationCardId(final String electionEventId,
			final String verificationCardId) {
		String sql = "select confirmation_attempts, exponentiation_computed from computed_verification_cards where election_event_id = ? and verification_card_id = ? ";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, electionEventId);
			statement.setString(2, verificationCardId);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.first()) {
					ComputedVerificationCard computedVerificationCard = new ComputedVerificationCard(electionEventId, verificationCardId);
					computedVerificationCard.setConfirmationAttempts(resultSet.getInt(1));
					computedVerificationCard.setExponentiationComputed(resultSet.getString(2).equals(TRUE));
					return Optional.of(computedVerificationCard);
				} else {
					return Optional.empty();
				}
			}
		} catch (SQLException e) {
			throw new IllegalStateException(FAILED_ACCESS_COMPUTED_VERIFICATION, e);
		}
	}

	public boolean existsById(final String electionEventId, final String verificationCardId) {
		String sql = "select election_event_id from computed_verification_cards where election_event_id = ? and verification_card_id = ? ";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, electionEventId);
			statement.setString(2, verificationCardId);
			try (ResultSet resultSet = statement.executeQuery()) {
				return resultSet.next();
			}
		} catch (SQLException e) {
			throw new IllegalStateException(FAILED_ACCESS_COMPUTED_VERIFICATION, e);
		}
	}

	public void updateComputedVerificationCard(final ComputedVerificationCard computedVerificationCard)
			throws ComputedVerificationCardNotFoundException {
		String sql = "update computed_verification_cards set confirmation_attempts = ?, exponentiation_computed = ? "
				+ "where election_event_id = ? and verification_card_id = ? ";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setInt(1, computedVerificationCard.getConfirmationAttempts());
			statement.setString(2, computedVerificationCard.isExponentiationComputed() ? TRUE : FALSE);
			statement.setString(3, computedVerificationCard.getElectionEventId());
			statement.setString(4, computedVerificationCard.getVerificationCardId());

			int modified = statement.executeUpdate();

			if (modified == 0) {
				throw new ComputedVerificationCardNotFoundException(
						NO_EXISTING_COMPUTED_VERIFICATION_CARD_ENTRY_WITH + verificationCardIdAndElectionEventIdStr(
								computedVerificationCard.getVerificationCardId(),
								computedVerificationCard.getElectionEventId()));
			}

		} catch (SQLException e) {
			throw new IllegalStateException(FAILED_ACCESS_COMPUTED_VERIFICATION, e);
		}
	}

	private String verificationCardIdAndElectionEventIdStr(String verificationCardId, String electionEventId) {
		return String.format("verificationCardId %s and electionEventId %s", verificationCardId, electionEventId);
	}
}