/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.certificates.utils.PemUtils;
import ch.post.it.evoting.cryptoprimitives.hashing.HashService;
import ch.post.it.evoting.domain.election.model.messaging.PayloadVerificationException;
import ch.post.it.evoting.domain.returncodes.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.LongReturnCodesSharePayload;
import ch.post.it.evoting.domain.returncodes.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.ReturnCodesInput;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.domain.model.platform.PlatformCARepository;
import ch.post.it.evoting.votingserver.commons.infrastructure.remote.client.RetrofitConsumer;
import ch.post.it.evoting.votingserver.commons.tracking.TrackIdInstance;
import ch.post.it.evoting.votingserver.voteverification.domain.model.platform.VvPlatformCARepository;
import ch.post.it.evoting.votingserver.voteverification.infrastructure.remote.OrchestratorClient;

import okhttp3.ResponseBody;

/**
 * Computes pre-Choice Return Codes or the pre-Vote Cast Return Code
 */
@Stateless
public class CodesComputeService {

	static final String CC_ORCHESTRATOR_PATH = "choicecodes";

	private static final Logger LOGGER = LoggerFactory.getLogger(CodesComputeService.class);

	@Inject
	private OrchestratorClient ccOrchestratorClient;

	@Inject
	private TrackIdInstance trackId;

	@Inject
	private CryptolibPayloadSignatureService payloadSignatureService;

	@Inject
	private HashService hashService;

	@Inject
	@VvPlatformCARepository
	private PlatformCARepository platformCARepository;

	@Inject
	private ObjectMapper mapper;

	/**
	 * Computes in collaboration with the control components the pre-Choice Return Codes or pre-Vote Cast Return Code.
	 *
	 * @param tenantId              - tenant identifier.
	 * @param eeId                  - election event identifier.
	 * @param verificationCardSetId - verification card set id
	 * @param verificationCardId    - verification card id
	 * @param returnCodesInput      - encrypted partial Choice Return Codes (for calculating the pre-Choice Return Codes) or the confirmation message
	 *                              (for calculating the pre-Vote Cast Return Code)
	 * @return a map with the pre-Choice Return Codes or the pre-Vote Cast Return Code
	 */
	public List<LongChoiceReturnCodesShare> computeLongChoiceReturnCodeShares(final String tenantId, final String eeId,
			final String verificationCardSetId, final String verificationCardId, final ReturnCodesInput returnCodesInput)
			throws ResourceNotFoundException {

		try {
			LOGGER.info("Requesting the long Choice Return Code shares...");
			return collectLongChoiceReturnCodeShares(tenantId, eeId, verificationCardSetId, verificationCardId, returnCodesInput);
		} catch (IOException e) {
			throw new UncheckedIOException("Failed to collect long Choice Return Code shares.", e);
		}
	}

	public List<LongVoteCastReturnCodesShare> computeLongVoteCastReturnCodeShares(final String tenantId, final String eeId,
			final String verificationCardSetId, final String verificationCardId, final ReturnCodesInput returnCodesInput)
			throws ResourceNotFoundException {

		try {
			LOGGER.info("Requesting the long Vote Cast Return Code shares...");
			return collectLongVoteCastReturnCodeShares(tenantId, eeId, verificationCardSetId, verificationCardId, returnCodesInput);
		} catch (IOException e) {
			throw new UncheckedIOException("Failed to collect long Vote Cast Return Code shares.", e);
		}
	}

	private List<LongChoiceReturnCodesShare> collectLongChoiceReturnCodeShares(final String tenantId, final String eeId,
			final String verificationCardSetId, final String verificationCardId, final ReturnCodesInput returnCodesInput)
			throws ResourceNotFoundException, IOException {

		try (ResponseBody response = RetrofitConsumer.processResponse(ccOrchestratorClient
				.getChoiceCodeNodesComputeContributions(trackId.getTrackId(), CC_ORCHESTRATOR_PATH, tenantId, eeId, verificationCardSetId,
						verificationCardId, returnCodesInput))) {
			// Choice Return Codes computation response correctly received.
			final byte[] responseBytes = response.bytes();
			final List<LongReturnCodesSharePayload> longChoiceReturnCodeShares = Arrays
					.asList(mapper.readValue(responseBytes, LongReturnCodesSharePayload[].class));
			LOGGER.info("Retrieved the long Choice Return Code shares payload.");

			verifyPayloadSignatures(tenantId, eeId, verificationCardId, longChoiceReturnCodeShares);

			return longChoiceReturnCodeShares.stream()
					.map(LongReturnCodesSharePayload::getLongReturnCodesShare)
					.map(LongChoiceReturnCodesShare.class::cast)
					.collect(Collectors.toList());
		}
	}

	private List<LongVoteCastReturnCodesShare> collectLongVoteCastReturnCodeShares(final String tenantId, final String eeId,
			final String verificationCardSetId, final String verificationCardId, final ReturnCodesInput returnCodesInput)
			throws ResourceNotFoundException, IOException {

		try (ResponseBody response = RetrofitConsumer.processResponse(ccOrchestratorClient
				.getChoiceCodeNodesComputeContributions(trackId.getTrackId(), CC_ORCHESTRATOR_PATH, tenantId, eeId, verificationCardSetId,
						verificationCardId, returnCodesInput))) {
			// Vote Cast Return Code computation response correctly received.
			final byte[] responseBytes = response.bytes();
			final List<LongReturnCodesSharePayload> longVoteCastReturnCodeShares = Arrays
					.asList(mapper.readValue(responseBytes, LongReturnCodesSharePayload[].class));
			LOGGER.info("Retrieved the Long Vote Cast Return Code share payload.");

			verifyPayloadSignatures(tenantId, eeId, verificationCardId, longVoteCastReturnCodeShares);

			return longVoteCastReturnCodeShares.stream()
					.map(LongReturnCodesSharePayload::getLongReturnCodesShare)
					.map(LongVoteCastReturnCodesShare.class::cast)
					.collect(Collectors.toList());
		}
	}

	private void verifyPayloadSignatures(final String tenantId, final String eeId, final String verificationCardId,
			final List<LongReturnCodesSharePayload> choiceCodesVerificationResults) {

		final X509Certificate rootCertificate;
		try {
			rootCertificate = (X509Certificate) PemUtils
					.certificateFromPem(platformCARepository.getRootCACertificate().getCertificateContent());
		} catch (GeneralCryptoLibException | ResourceNotFoundException e) {
			throw new IllegalArgumentException("Failed to retrieve root certificate to verify payload signature.", e);
		}

		for (final LongReturnCodesSharePayload payload : choiceCodesVerificationResults) {
			final byte[] payloadHash = hashService.recursiveHash(payload);

			final boolean signatureValid;
			try {
				signatureValid = payloadSignatureService.verify(payload.getSignature(), rootCertificate, payloadHash);
			} catch (PayloadVerificationException e) {
				throw new IllegalArgumentException("Failed to verify payload signature.", e);
			}

			if (!signatureValid) {
				throw new IllegalArgumentException(
						"Signature invalid for tenantId " + tenantId + " eeId " + eeId + " verificationCardId " + verificationCardId);
			}
		}
	}

}
