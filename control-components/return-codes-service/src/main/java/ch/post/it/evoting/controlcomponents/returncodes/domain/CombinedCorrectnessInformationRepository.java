/*
 * (c) Copyright 2020 Swiss Post Ltd.
 */

package ch.post.it.evoting.controlcomponents.returncodes.domain;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.UUIDValidations.validateUUID;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.CombinedCorrectnessInformationRepositoryException;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;

@Repository
public class CombinedCorrectnessInformationRepository {

	private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private static final String SAVE_COMBINED_CORRECTNESS_INFORMATION_SQL =
			"INSERT INTO CC_COMBINED_CORRECTNESS_INFORMATION (ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, COMBINED_CORRECTNESS_INFORMATION) VALUES (?,?,?)";

	private static final String FIND_BY_ELECTION_EVENT_ID_AND_VERIFICATION_CARD_SET_ID =
			"SELECT COMBINED_CORRECTNESS_INFORMATION FROM CC_COMBINED_CORRECTNESS_INFORMATION WHERE ELECTION_EVENT_ID=? AND VERIFICATION_CARD_SET_ID=?";

	@Autowired
	private DataSource dataSource;

	public void saveCombinedCorrectnessInformation(final String electionEventId, final String verificationCardSetId,
			final CombinedCorrectnessInformation combinedCorrectnessInformation) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);
		checkNotNull(combinedCorrectnessInformation, "The provided combined correctness information is null.");

		try (final Connection connection = dataSource.getConnection();
				final PreparedStatement statement = connection.prepareStatement(SAVE_COMBINED_CORRECTNESS_INFORMATION_SQL)) {
			statement.setString(1, electionEventId);
			statement.setString(2, verificationCardSetId);
			statement.setBytes(3, OBJECT_MAPPER.writeValueAsString(combinedCorrectnessInformation).getBytes(StandardCharsets.UTF_8));

			statement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new CombinedCorrectnessInformationRepositoryException(String.format(
					"Already existing combined correctness information entry with election event id %s and verification card set id %s.",
					electionEventId, verificationCardSetId), e);
		} catch (SQLException | JsonProcessingException e) {
			throw new CombinedCorrectnessInformationRepositoryException(String.format(
					"An error occurred while saving the provided combined correctness information entry with election event id %s and verification card set id %s.",
					electionEventId, verificationCardSetId), e);
		}
	}

	public CombinedCorrectnessInformation findByElectionEventIdAndVerificationCardSetId(final String electionEventId,
			final String verificationCardSetId) {

		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		try (final Connection connection = dataSource.getConnection();
				final PreparedStatement statement = connection.prepareStatement(FIND_BY_ELECTION_EVENT_ID_AND_VERIFICATION_CARD_SET_ID)) {
			statement.setString(1, electionEventId);
			statement.setString(2, verificationCardSetId);

			try (final ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {
					return OBJECT_MAPPER.readValue(new String(resultSet.getBytes(1), StandardCharsets.UTF_8), CombinedCorrectnessInformation.class);
				} else {
					throw new CombinedCorrectnessInformationRepositoryException(
							String.format("No combined correctness information found for election event id %s and verification card set id %s.",
									electionEventId, verificationCardSetId));
				}
			}
		} catch (SQLException | JsonProcessingException e) {
			throw new CombinedCorrectnessInformationRepositoryException(String.format(
					"An error occurred while retrieving the combined correctness information for election event id %s and verification card set id %s.",
					electionEventId, verificationCardSetId), e);
		}
	}

	public boolean existsByElectionEventIdAndVerificationCardSetId(final String electionEventId, final String verificationCardSetId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardSetId);

		try (final Connection connection = dataSource.getConnection();
				final PreparedStatement statement = connection.prepareStatement(FIND_BY_ELECTION_EVENT_ID_AND_VERIFICATION_CARD_SET_ID)) {
			statement.setString(1, electionEventId);
			statement.setString(2, verificationCardSetId);

			try (final ResultSet resultSet = statement.executeQuery()) {
				return resultSet.next();
			}
		} catch (SQLException e) {
			throw new CombinedCorrectnessInformationRepositoryException(String.format(
					"An error occurred while searching a combined correctness information for election event id %s and verification card set id %s.",
					electionEventId, verificationCardSetId), e);
		}

	}
}
