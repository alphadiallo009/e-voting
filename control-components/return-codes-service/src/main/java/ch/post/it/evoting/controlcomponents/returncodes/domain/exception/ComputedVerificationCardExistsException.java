/*
 * (c) Copyright 2020 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain.exception;

public class ComputedVerificationCardExistsException extends Exception {
	private static final long serialVersionUID = 1L;

	public ComputedVerificationCardExistsException(String msg, Throwable t) {
		super(msg, t);
	}
}


