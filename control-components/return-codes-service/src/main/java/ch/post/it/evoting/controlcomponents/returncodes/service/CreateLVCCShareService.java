/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.service;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.common.annotations.VisibleForTesting;

import ch.post.it.evoting.controlcomponents.returncodes.domain.ComputedVerificationCard;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ComputedVerificationCardsRepository;
import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.ComputedVerificationCardNotFoundException;
import ch.post.it.evoting.controlcomponents.returncodes.service.exception.KeyDerivationException;
import ch.post.it.evoting.controlcomponents.returncodes.service.exception.MissingComputeVerificationCardException;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.Exponent;
import ch.post.it.evoting.cryptoprimitives.GroupVector;
import ch.post.it.evoting.cryptoprimitives.hashing.HashService;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.domain.cryptoadapters.CryptoAdapters;

/**
 * Implements the CreateLVCCShare_j algorithm.
 */
@Service
public class CreateLVCCShareService {

	@VisibleForTesting
	static final int MAX_CONFIRMATION_ATTEMPTS = 5;

	private static final Logger LOGGER = LoggerFactory.getLogger(CreateLVCCShareService.class);

	private final int nodeID;
	private final HashService hashService;
	private final ZeroKnowledgeProof zeroKnowledgeProofService;
	private final ComputedVerificationCardsRepository computedVerificationCardsRepository;
	private final VoterReturnCodeGenerationKeyDerivationService voterReturnCodeGenerationKeyDerivationService;

	public CreateLVCCShareService(
			@Value("${nodeID}")
			final int nodeID, final HashService hashService,
			final ZeroKnowledgeProof zeroKnowledgeProofService,
			final ComputedVerificationCardsRepository computedVerificationCardsRepository,
			final VoterReturnCodeGenerationKeyDerivationService voterReturnCodeGenerationKeyDerivationService) {

		this.nodeID = nodeID;
		this.hashService = hashService;
		this.zeroKnowledgeProofService = zeroKnowledgeProofService;
		this.computedVerificationCardsRepository = computedVerificationCardsRepository;
		this.voterReturnCodeGenerationKeyDerivationService = voterReturnCodeGenerationKeyDerivationService;
	}

	/**
	 * Generates the long Vote Cast Return Code shares.
	 *
	 * @param confirmationKey                    CK<sub>id</sub>, the voter confirmation key.
	 * @param ccrjReturnCodesGenerationSecretKey k'<sub>j</sub>∈ Z<sub>q</sub>, CCR<sub>j</sub> return codes generation secret key. Not null.
	 * @param verificationCardId                 vc<sub>id</sub>, the verification card id. Not null.
	 * @param electionEventId                    the corresponding election event id. Not null.
	 * @return the long vote cast return code share, voter vote cast return code generation public key and exponentiation proof encapsulated in a
	 * {@link CreateLVCCShareOutput}.
	 * @throws NullPointerException                      if any input parameter is null.
	 * @throws IllegalArgumentException                  if
	 *                                                   <ul>
	 *                                                       <li>The confirmation and secret keys do not have the same group order.</li>
	 *                                                       <li>The verification card is not in L_sentVotes,j.</li>
	 *                                                   </ul>
	 * @throws ComputedVerificationCardNotFoundException if the computed verification card to update could not be found.
	 */
	@SuppressWarnings("java:S117")
	public CreateLVCCShareOutput createLVCCShare(final GqElement confirmationKey, final ZqElement ccrjReturnCodesGenerationSecretKey,
			final String verificationCardId, final String electionEventId) throws ComputedVerificationCardNotFoundException {

		checkNotNull(confirmationKey);
		checkNotNull(ccrjReturnCodesGenerationSecretKey);
		checkNotNull(verificationCardId);
		checkNotNull(electionEventId);

		// Cross group checks.
		checkArgument(confirmationKey.getGroup().hasSameOrderAs(ccrjReturnCodesGenerationSecretKey.getGroup()),
				"Confirmation key and CCR_j Return Codes Generation secret key must have the same group order.");

		// Ensure vc_id ∈ L_sentVotes,j.
		final ComputedVerificationCard computedVerificationCard = getComputedVerificationCard(electionEventId, verificationCardId);
		checkArgument(computedVerificationCard.isExponentiationComputed(),
				String.format("The CCR_j did not compute the long Choice Return Code shares for verification card %s.", verificationCardId));

		// Variables.
		final GqElement g = confirmationKey.getGroup().getGenerator();
		final String vc_id = verificationCardId;
		final GqElement CK_id = confirmationKey;
		final String ee = electionEventId;

		// Operation.
		final int attempts_id = computedVerificationCard.getConfirmationAttempts();
		checkArgument(attempts_id < MAX_CONFIRMATION_ATTEMPTS, String.format("Max confirmation attempts of %s exceeded.", MAX_CONFIRMATION_ATTEMPTS));

		final ZqElement kc_j_id = deriveKey(vc_id, ccrjReturnCodesGenerationSecretKey);
		final GqElement Kc_j_id = g.exponentiate(kc_j_id);
		final GqElement hCK_id = CK_id.hashAndSquare(hashService);
		final GqElement lVCC_id = hCK_id.exponentiate(kc_j_id);

		final List<String> i_aux = Arrays.asList(ee, vc_id, "CreateLVCCShare", String.valueOf(nodeID), String.valueOf(attempts_id));
		final GroupVector<GqElement, GqGroup> bases = GroupVector.of(g, hCK_id);
		final GroupVector<GqElement, GqGroup> exponentiations = GroupVector.of(Kc_j_id, lVCC_id);
		final ExponentiationProof pi_expLVCC_j_id = zeroKnowledgeProofService.genExponentiationProof(bases, kc_j_id, exponentiations, i_aux);

		computedVerificationCard.setConfirmationAttempts(attempts_id + 1);
		computedVerificationCardsRepository.updateComputedVerificationCard(computedVerificationCard);

		return new CreateLVCCShareOutput(attempts_id + 1, hCK_id, lVCC_id, Kc_j_id, pi_expLVCC_j_id);
	}

	private ZqElement deriveKey(final String verificationCardId, final ZqElement ccrjReturnCodesGenerationSecretKey) {

		final Exponent exponent = CryptoAdapters.convert(ccrjReturnCodesGenerationSecretKey);
		final String seed = verificationCardId + "confirm";
		final BigInteger q = ccrjReturnCodesGenerationSecretKey.getGroup().getQ();
		final Exponent voterVoteCastReturnCodeGenerationSecretKey;

		try {
			voterVoteCastReturnCodeGenerationSecretKey = voterReturnCodeGenerationKeyDerivationService
					.deriveVoterReturnCodeGenerationPrivateKey(exponent, seed, q);
			LOGGER.info("Voter Vote Cast Return Code Generation secret key successfully derived for verificationCardId {}", verificationCardId);
		} catch (GeneralCryptoLibException e) {
			LOGGER.error("The derivation of the private key has failed for verificationCardId {}", verificationCardId);
			throw new KeyDerivationException(verificationCardId);
		}

		return CryptoAdapters.convert(voterVoteCastReturnCodeGenerationSecretKey);
	}

	private ComputedVerificationCard getComputedVerificationCard(final String electionEventId, final String verificationCardId) {
		final Optional<ComputedVerificationCard> optionalComputedVerificationCard = computedVerificationCardsRepository.findByElectionEventIdAndVerificationCardId(
				electionEventId, verificationCardId);

		if (!optionalComputedVerificationCard.isPresent()) {
			LOGGER.error("Missing verification card for electionEventId {} and verificationCardId {}", electionEventId, verificationCardId);
			throw new MissingComputeVerificationCardException(electionEventId, verificationCardId);
		}
		return optionalComputedVerificationCard.get();
	}

	public static class CreateLVCCShareOutput {

		private final int confirmationAttempts;
		private final GqElement hashedSquaredConfirmationKey;
		private final GqElement longVoteCastReturnCodeShare;
		private final GqElement voterVoteCastReturnCodeGenerationPublicKey;
		private final ExponentiationProof exponentiationProof;

		CreateLVCCShareOutput(final int confirmationAttempts, final GqElement hashedSquaredConfirmationKey,
				final GqElement longVoteCastReturnCodeShare, final GqElement voterVoteCastReturnCodeGenerationPublicKey,
				final ExponentiationProof exponentiationProof) {

			checkArgument(confirmationAttempts >= 0);
			checkArgument(confirmationAttempts < MAX_CONFIRMATION_ATTEMPTS);
			checkNotNull(hashedSquaredConfirmationKey);
			checkNotNull(longVoteCastReturnCodeShare);
			checkNotNull(voterVoteCastReturnCodeGenerationPublicKey);
			checkNotNull(exponentiationProof);

			// Cross group checks.
			checkArgument(hashedSquaredConfirmationKey.getGroup().equals(longVoteCastReturnCodeShare.getGroup()),
					"Confirmation key and long vote cast return code share must have the same group.");
			checkArgument(hashedSquaredConfirmationKey.getGroup().equals(voterVoteCastReturnCodeGenerationPublicKey.getGroup()),
					"Confirmation key and Voter Vote Cast Return Code Generation public key must have the same group.");
			checkArgument(hashedSquaredConfirmationKey.getGroup().hasSameOrderAs(exponentiationProof.getGroup()),
					"Confirmation key and exponentiation proof must have the same group order.");

			this.confirmationAttempts = confirmationAttempts;
			this.hashedSquaredConfirmationKey = hashedSquaredConfirmationKey;
			this.longVoteCastReturnCodeShare = longVoteCastReturnCodeShare;
			this.voterVoteCastReturnCodeGenerationPublicKey = voterVoteCastReturnCodeGenerationPublicKey;
			this.exponentiationProof = exponentiationProof;
		}

		public int getConfirmationAttempts() {
			return confirmationAttempts;
		}

		public GqElement getHashedSquaredConfirmationKey() {
			return hashedSquaredConfirmationKey;
		}

		public GqElement getLongVoteCastReturnCodeShare() {
			return longVoteCastReturnCodeShare;
		}

		public GqElement getVoterVoteCastReturnCodeGenerationPublicKey() {
			return voterVoteCastReturnCodeGenerationPublicKey;
		}

		public ExponentiationProof getExponentiationProof() {
			return exponentiationProof;
		}
	}
}
