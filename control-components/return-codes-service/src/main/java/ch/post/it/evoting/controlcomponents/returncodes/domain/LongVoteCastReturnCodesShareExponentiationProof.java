/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.UUIDValidations.validateUUID;

import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

/**
 * Encapsulates an exponentiation proof for the exponentiation of the hashed and squared confirmation key with the voter vote cast return code
 * generation secret key. This object also includes the bases and exponentiated elements. However to avoid redundancy, object does not contain the
 * public key and the context information that is necessary to verify the proofs.
 */
public class LongVoteCastReturnCodesShareExponentiationProof {

	private String verificationCardId;
	private int confirmationAttempt;
	private GqElement hashedConfirmationKey;
	private GqElement ccrjLongVoteCastReturnCodeShare;
	private ExponentiationProof exponentiationProof;

	public LongVoteCastReturnCodesShareExponentiationProof(final String verificationCardId, final int confirmationAttempt,
			final GqElement hashedConfirmationKey, final GqElement ccrjLongVoteCastReturnCodeShare, final ExponentiationProof exponentiationProof) {

		validateUUID(verificationCardId);
		this.verificationCardId = verificationCardId;
		this.confirmationAttempt = confirmationAttempt;
		this.hashedConfirmationKey = hashedConfirmationKey;
		this.ccrjLongVoteCastReturnCodeShare = ccrjLongVoteCastReturnCodeShare;
		this.exponentiationProof = exponentiationProof;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public void setVerificationCardId(String verificationCardId) {
		this.verificationCardId = verificationCardId;
	}

	public int getConfirmationAttempt() {
		return confirmationAttempt;
	}

	public void setConfirmationAttempt(int confirmationAttempt) {
		this.confirmationAttempt = confirmationAttempt;
	}

	public GqElement getHashedConfirmationKey() {
		return hashedConfirmationKey;
	}

	public void setHashedConfirmationKey(GqElement hashedConfirmationKey) {
		this.hashedConfirmationKey = hashedConfirmationKey;
	}

	public GqElement getCcrjLongVoteCastReturnCodeShare() {
		return ccrjLongVoteCastReturnCodeShare;
	}

	public void setCcrjLongVoteCastReturnCodeShare(GqElement ccrjLongVoteCastReturnCodeShare) {
		this.ccrjLongVoteCastReturnCodeShare = ccrjLongVoteCastReturnCodeShare;
	}

	public ExponentiationProof getExponentiationProof() {
		return exponentiationProof;
	}

	public void setExponentiationProof(ExponentiationProof exponentiationProof) {
		this.exponentiationProof = exponentiationProof;
	}
}
