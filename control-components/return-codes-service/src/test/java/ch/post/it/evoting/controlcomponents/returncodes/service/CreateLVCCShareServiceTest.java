/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.service;

import static ch.post.it.evoting.controlcomponents.returncodes.service.CreateLVCCShareService.CreateLVCCShareOutput;
import static ch.post.it.evoting.controlcomponents.returncodes.service.CreateLVCCShareService.MAX_CONFIRMATION_ATTEMPTS;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.controlcomponents.returncodes.domain.ComputedVerificationCard;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ComputedVerificationCardsRepository;
import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.ComputedVerificationCardNotFoundException;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.Exponent;
import ch.post.it.evoting.cryptoprimitives.CryptoPrimitivesService;
import ch.post.it.evoting.cryptoprimitives.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.hashing.HashService;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofService;
import ch.post.it.evoting.domain.cryptoadapters.CryptoAdapters;

@DisplayName("CreateLVCCShareService")
class CreateLVCCShareServiceTest extends TestGroupSetup {

	private static final int NODE_ID = 1;
	private static final int l_ID = 32;
	private static final HashService hashService = spy(HashService.class);
	private static final ZeroKnowledgeProof zeroKnowledgeProofService = spy(ZeroKnowledgeProofService.class);
	private static final ComputedVerificationCardsRepository computedVerificationCardRepositoryMock = mock(ComputedVerificationCardsRepository.class);
	private static final VoterReturnCodeGenerationKeyDerivationService voterReturnCodeGenerationKeyDerivationServiceMock = mock(
			VoterReturnCodeGenerationKeyDerivationService.class);

	private static CreateLVCCShareService createLVCCShareService;

	@BeforeAll
	static void setUpAll() {
		createLVCCShareService = new CreateLVCCShareService(NODE_ID, hashService, zeroKnowledgeProofService, computedVerificationCardRepositoryMock,
				voterReturnCodeGenerationKeyDerivationServiceMock);
	}

	@Nested
	@DisplayName("calling createLVCCShare with")
	class CreateLVCC {

		private GqElement confirmationKey;
		private ZqElement ccrjReturnCodesGenerationSecretKey;
		private String verificationCardId;
		private String electionEventId;

		@BeforeEach
		void setUp() {
			confirmationKey = gqGroupGenerator.genMember();
			ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();
			verificationCardId = CryptoPrimitivesService.get().genRandomBase16String(l_ID).toLowerCase();
			electionEventId = CryptoPrimitivesService.get().genRandomBase16String(l_ID).toLowerCase();
		}

		@Test
		@DisplayName("valid parameters does not throw")
		void validParameters() throws GeneralCryptoLibException, ComputedVerificationCardNotFoundException {
			final ComputedVerificationCard computedVerificationCard = new ComputedVerificationCard(electionEventId, verificationCardId);
			computedVerificationCard.setExponentiationComputed(true);
			computedVerificationCard.setConfirmationAttempts(0);
			when(computedVerificationCardRepositoryMock.findByElectionEventIdAndVerificationCardId(electionEventId, verificationCardId)).thenReturn(
					Optional.of(computedVerificationCard));

			final Exponent exponent = CryptoAdapters.convert(ccrjReturnCodesGenerationSecretKey);
			final Exponent derivedKey = CryptoAdapters.convert(zqGroupGenerator.genRandomZqElementMember());
			when(voterReturnCodeGenerationKeyDerivationServiceMock
					.deriveVoterReturnCodeGenerationPrivateKey(exponent, verificationCardId + "confirm", gqGroup.getQ())).thenReturn(derivedKey);

			doReturn(0).when(hashService).getHashLength();
			doReturn(new byte[] { 0x4 }).when(hashService).recursiveHash(any());

			final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
					zqGroupGenerator.genRandomZqElementMember());
			doReturn(exponentiationProof).when(zeroKnowledgeProofService).genExponentiationProof(any(), any(), any(), any());

			doNothing().when(computedVerificationCardRepositoryMock).updateComputedVerificationCard(computedVerificationCard);

			final CreateLVCCShareOutput output = createLVCCShareService.createLVCCShare(confirmationKey, ccrjReturnCodesGenerationSecretKey,
					verificationCardId, electionEventId);

			assertEquals(1, output.getConfirmationAttempts());
			assertEquals(gqGroup, output.getHashedSquaredConfirmationKey().getGroup());
			assertEquals(gqGroup, output.getLongVoteCastReturnCodeShare().getGroup());
			assertEquals(gqGroup, output.getVoterVoteCastReturnCodeGenerationPublicKey().getGroup());
			assertTrue(gqGroup.hasSameOrderAs(output.getExponentiationProof().getGroup()));
		}

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void nullParameters() {
			assertAll(
					() -> assertThrows(NullPointerException.class,
							() -> createLVCCShareService.createLVCCShare(null, ccrjReturnCodesGenerationSecretKey, verificationCardId,
									electionEventId)),
					() -> assertThrows(NullPointerException.class,
							() -> createLVCCShareService.createLVCCShare(confirmationKey, null, verificationCardId, electionEventId)),
					() -> assertThrows(NullPointerException.class,
							() -> createLVCCShareService.createLVCCShare(confirmationKey, ccrjReturnCodesGenerationSecretKey, null, electionEventId)),
					() -> assertThrows(NullPointerException.class,
							() -> createLVCCShareService.createLVCCShare(confirmationKey, ccrjReturnCodesGenerationSecretKey, verificationCardId,
									null))
			);
		}

		@Test
		@DisplayName("confirmation key and secret key having different group order throws IllegalArgumentException")
		void diffGroupKeys() {
			final GqElement otherGroupConfirmationKey = otherGqGroupGenerator.genMember();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLVCCShareService.createLVCCShare(otherGroupConfirmationKey, ccrjReturnCodesGenerationSecretKey, verificationCardId,
							electionEventId));
			assertEquals("Confirmation key and CCR_j Return Codes Generation secret key must have the same group order.", exception.getMessage());
		}

		@Test
		@DisplayName("long choice return codes not computed throw IllegalArgumentException")
		void notPreviouslyComputedLCC() {
			final ComputedVerificationCard verificationCard = new ComputedVerificationCard(electionEventId, verificationCardId);
			verificationCard.setExponentiationComputed(false);
			when(computedVerificationCardRepositoryMock.findByElectionEventIdAndVerificationCardId(electionEventId, verificationCardId)).thenReturn(
					Optional.of(verificationCard));

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLVCCShareService.createLVCCShare(confirmationKey, ccrjReturnCodesGenerationSecretKey, verificationCardId,
							electionEventId));
			final String message = String.format("The CCR_j did not compute the long Choice Return Code shares for verification card %s.",
					verificationCardId);
			assertEquals(message, exception.getMessage());
		}

		@Test
		@DisplayName("max confirmation attempts exceeded throws IllegalArgumentException")
		void exceededAttempts() {
			final ComputedVerificationCard verificationCard = new ComputedVerificationCard(electionEventId, verificationCardId);
			verificationCard.setExponentiationComputed(true);
			verificationCard.setConfirmationAttempts(MAX_CONFIRMATION_ATTEMPTS + 1);
			when(computedVerificationCardRepositoryMock.findByElectionEventIdAndVerificationCardId(electionEventId, verificationCardId)).thenReturn(
					Optional.of(verificationCard));

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLVCCShareService.createLVCCShare(confirmationKey, ccrjReturnCodesGenerationSecretKey, verificationCardId,
							electionEventId));
			assertEquals(String.format("Max confirmation attempts of %s exceeded.", MAX_CONFIRMATION_ATTEMPTS), exception.getMessage());
		}

	}

}