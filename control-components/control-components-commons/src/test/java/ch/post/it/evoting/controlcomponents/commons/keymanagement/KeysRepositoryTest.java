/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.commons.keymanagement;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.security.KeyManagementException;
import java.security.KeyStore.PasswordProtection;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.controlcomponents.commons.keymanagement.exception.KeyAlreadyExistsException;
import ch.post.it.evoting.controlcomponents.commons.keymanagement.exception.KeyNotFoundException;
import ch.post.it.evoting.cryptolib.api.elgamal.ElGamalServiceAPI;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalEncryptionParameters;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalKeyPair;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalPrivateKey;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalPublicKey;
import ch.post.it.evoting.cryptolib.elgamal.service.ElGamalService;

class KeysRepositoryTest {

	private static final String NODE_ID = "nodeId";
	private static final String ELECTION_EVENT_ID = "0b88257ec32142bb8ee0ed1bb70f362e";
	private static final String VERIFICATION_CARD_SET_ID = "ffbf3c4fd4314309b5988b3df2668a2c";
	private static final String ENCRYPTION_PARAMETERS_JSON = "{\"encryptionParams\":{\"p\":\"AMoeTsDNAOCZ1I+tv3/xjI/HVT8Ab3h8Ifm"
			+ "/d1623VoBQaL8mzFgtUerEiw/FcjyUnjwvu0cc2kPR6uM4eMgK6c7YXE/LSJ2BDgNMVLUgIIC+qVZLiEG/yPZAT"
			+ "+8akHS0Audz7XIxBnd1EzqsnkIFjsgtER3iVWpC7BTxJ1sT19pB+OaA2b"
			+ "+L6nVW0sZNwiHFCvvQ5rLTEc4uDEBcT6aNIXCK4Qkztc1hTfccn3k6158NNGMddvBB5GzqBoQMzjW35mKUBXOufRhQTYVQjNSoapfVfVKDsTkD1BDvdffJC/gP82m"
			+ "+/kud0x0p6RWfmfHgFoNmG6EjJkoMGfGLVJAMVs=\",\"q\":\"ZQ8nYGaAcEzqR9bfv/jGR+Oqn4A3vD4Q/N+7r1turQCg0X5NmLBao9WJFh"
			+ "+K5HkpPHhfdo45tIej1cZw8ZAV052wuJ+WkTsCHAaYqWpAQQF9UqyXEIN"
			+ "/keyAn941IOloBc7n2uRiDO7qJnVZPIQLHZBaIjvEqtSF2CniTrYnr7SD8c0Bs38X1OqtpYybhEOKFfehzWWmI5xcGIC4n00aQuEVwhJna5rCm"
			+ "+45PvJ1rz4aaMY67eCDyNnUDQgZnGtvzMUoCudc+jCgmwqhGalQ1S+q+qUHYnIHqCHe6++SF/Af5tN9/Jc7pjpT0is/M+PALQbMN0JGTJQYM+MWqSAYrQ==\","
			+ "\"g\":\"Aw==\"}}";

	private static ElGamalPrivateKey elGamalPrivateKey;
	private static ElGamalPublicKey elGamalPublicKey;

	private final PasswordProtection password = new PasswordProtection("password".toCharArray());
	private final Codec codec = mock(Codec.class);
	private final Generator generator = mock(Generator.class);
	private final PrivateKey encryptionPrivateKey = mock(PrivateKey.class);
	private final PublicKey encryptionPublicKey = mock(PublicKey.class);

	private ResultSet resultSet;
	private PreparedStatement preparedStatement;
	private Connection connection;
	private KeysRepository keysRepository;

	@BeforeAll
	public static void beforeClass() throws GeneralCryptoLibException {
		ElGamalServiceAPI elGamalService = new ElGamalService();
		ElGamalEncryptionParameters parameters = ElGamalEncryptionParameters.fromJson(ENCRYPTION_PARAMETERS_JSON);
		ElGamalKeyPair pair = elGamalService.generateKeyPair(parameters, 1);
		elGamalPrivateKey = pair.getPrivateKeys();
		elGamalPublicKey = pair.getPublicKeys();
	}

	@BeforeEach
	public void setUp() throws KeyManagementException, SQLException {
		when(generator.generatePassword()).thenReturn(password);

		resultSet = mock(ResultSet.class);
		when(resultSet.next()).thenReturn(true);
		when(resultSet.getInt(1)).thenReturn(0);

		preparedStatement = mock(PreparedStatement.class);
		when(preparedStatement.executeQuery()).thenReturn(resultSet);

		connection = mock(Connection.class);
		when(connection.createStatement()).thenReturn(mock(Statement.class));
		when(connection.prepareStatement(anyString())).thenReturn(preparedStatement);

		DataSource dataSource = mock(DataSource.class);
		when(dataSource.getConnection()).thenReturn(connection);

		when(generator.generatePassword()).thenReturn(password);

		keysRepository = new KeysRepository(dataSource, codec, generator, NODE_ID);

		keysRepository.setEncryptionKeys(encryptionPrivateKey, encryptionPublicKey);
	}

	@Test
	void testLoadCcrjReturnCodesKeys() throws KeyManagementException, SQLException {
		byte[] publicKeySignature = { 1, 2, 3 };

		byte[] generationPrivateKeyBytes = { 1 };
		byte[] generationPublicKeyBytes = { 2 };
		byte[] decryptionPrivateKeyBytes = { 3 };
		byte[] decryptionPublicKeyBytes = { 4 };

		when(resultSet.getBytes("CCRJ_RETURN_CODES_GENERATION_SECRET_KEY")).thenReturn(generationPrivateKeyBytes);
		when(resultSet.getBytes("CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY")).thenReturn(generationPublicKeyBytes);
		when(resultSet.getBytes("CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY_SIGNATURE")).thenReturn(publicKeySignature);

		when(resultSet.getBytes("CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_SECRET_KEY")).thenReturn(decryptionPrivateKeyBytes);
		when(resultSet.getBytes("CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY")).thenReturn(decryptionPublicKeyBytes);
		when(resultSet.getBytes("CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY_SIGNATURE")).thenReturn(publicKeySignature);

		when(codec.decodeElGamalPrivateKey(generationPrivateKeyBytes, encryptionPrivateKey)).thenReturn(elGamalPrivateKey);
		when(codec.decodeElGamalPublicKey(generationPublicKeyBytes)).thenReturn(elGamalPublicKey);
		when(codec.decodeElGamalPrivateKey(decryptionPrivateKeyBytes, encryptionPrivateKey)).thenReturn(elGamalPrivateKey);
		when(codec.decodeElGamalPublicKey(decryptionPublicKeyBytes)).thenReturn(elGamalPublicKey);

		CcrjReturnCodesKeys ccrjReturnCodesKeys = keysRepository.loadCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID);

		assertEquals(elGamalPrivateKey, ccrjReturnCodesKeys.getCcrjReturnCodesGenerationSecretKey());
		assertEquals(elGamalPublicKey, ccrjReturnCodesKeys.getCcrjReturnCodesGenerationPublicKey());
		assertArrayEquals(publicKeySignature, ccrjReturnCodesKeys.getCcrjReturnCodesGenerationPublicKeySignature());
		assertEquals(elGamalPrivateKey, ccrjReturnCodesKeys.getCcrjChoiceReturnCodesEncryptionSecretKey());
		assertEquals(elGamalPublicKey, ccrjReturnCodesKeys.getCcrjChoiceReturnCodesEncryptionPublicKey());
		assertArrayEquals(publicKeySignature, ccrjReturnCodesKeys.getCcrjChoiceReturnCodesEncryptionPublicKeySignature());

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
		verify(preparedStatement).setString(1, NODE_ID);
		verify(preparedStatement).setString(2, ELECTION_EVENT_ID);
		verify(preparedStatement).setString(3, VERIFICATION_CARD_SET_ID);
	}

	@Test
	void testLoadCcrjReturnCodesKeysCodecException() throws KeyManagementException, SQLException {
		when(resultSet.getBytes(anyString())).thenReturn(new byte[0]);
		when(codec.decodeElGamalPrivateKey(any(byte[].class), eq(encryptionPrivateKey))).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));
	}

	@Test
	void testLoadCcrjReturnCodesKeysNotFound() throws SQLException {
		when(resultSet.next()).thenReturn(false);

		assertThrows(KeyNotFoundException.class, () -> keysRepository.loadCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadCcrjReturnCodesKeysSQLException() throws SQLException {
		when(resultSet.next()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadElectionSigningKeys() throws SQLException, KeyManagementException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };

		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		byte[] keysBytes = { 1, 2, 3 };
		byte[] passwordBytes = { 4, 5, 6 };

		when(resultSet.getBytes("keys")).thenReturn(keysBytes);
		when(resultSet.getBytes("password")).thenReturn(passwordBytes);

		when(codec.decodePassword(passwordBytes, encryptionPrivateKey)).thenReturn(password);
		when(codec.decodeElectionSigningKeys(keysBytes, password)).thenReturn(electionSigningKeys);

		assertEquals(electionSigningKeys, keysRepository.loadElectionSigningKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
		verify(preparedStatement).setString(1, NODE_ID);
		verify(preparedStatement).setString(2, ELECTION_EVENT_ID);
		assertTrue(password.isDestroyed());
	}

	@Test
	void testLoadElectionSigningKeysCodecException() throws SQLException, KeyManagementException {
		when(resultSet.getBytes("keys")).thenReturn(new byte[0]);
		when(resultSet.getBytes("password")).thenReturn(new byte[0]);
		when(codec.decodePassword(any(byte[].class), eq(encryptionPrivateKey))).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadElectionSigningKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadElectionSigningKeysNotFound() throws SQLException {
		when(resultSet.next()).thenReturn(false);

		assertThrows(KeyNotFoundException.class, () -> keysRepository.loadElectionSigningKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadElectionSigningKeysSQLException() throws SQLException {
		when(resultSet.next()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadElectionSigningKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadCcmjElectionKeys() throws SQLException, KeyManagementException {
		byte[] ccmjElectionPublicKeySignature = { 1, 2, 3 };

		byte[] ccmjElectionSecretKey = { 1 };
		byte[] ccmjElectionPublicKey = { 2 };

		when(resultSet.getBytes("CCMJ_ELECTION_SECRET_KEY")).thenReturn(ccmjElectionSecretKey);
		when(resultSet.getBytes("CCMJ_ELECTION_PUBLIC_KEY")).thenReturn(ccmjElectionPublicKey);
		when(resultSet.getBytes("CCMJ_ELECTION_PUBLIC_KEY_SIGNATURE")).thenReturn(ccmjElectionPublicKeySignature);

		when(codec.decodeElGamalPrivateKey(ccmjElectionSecretKey, encryptionPrivateKey)).thenReturn(elGamalPrivateKey);
		when(codec.decodeElGamalPublicKey(ccmjElectionPublicKey)).thenReturn(elGamalPublicKey);

		CcmjElectionKeys ccmjElectionKeys = keysRepository.loadCcmjElectionKeys(ELECTION_EVENT_ID);

		assertEquals(elGamalPrivateKey, ccmjElectionKeys.getCcmjElectionSecretKey());
		assertEquals(elGamalPublicKey, ccmjElectionKeys.getCcmjElectionPublicKey());
		assertArrayEquals(ccmjElectionPublicKeySignature, ccmjElectionKeys.getCcmjElectionPublicKeySignature());

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
		verify(preparedStatement).setString(1, NODE_ID);
		verify(preparedStatement).setString(2, ELECTION_EVENT_ID);
	}

	@Test
	void testLoadCcmjElectionKeysCodecException() throws SQLException, KeyManagementException {
		when(resultSet.getBytes("CCMJ_ELECTION_SECRET_KEY")).thenReturn(new byte[0]);
		when(resultSet.getBytes("CCMJ_ELECTION_PUBLIC_KEY")).thenReturn(new byte[0]);
		when(resultSet.getBytes("CCMJ_ELECTION_PUBLIC_KEY_SIGNATURE")).thenReturn(new byte[0]);

		when(codec.decodeElGamalPrivateKey(any(byte[].class), eq(encryptionPrivateKey))).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadCcmjElectionKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadCcmjElectionKeysNotFound() throws SQLException {
		when(resultSet.next()).thenReturn(false);

		assertThrows(KeyNotFoundException.class, () -> keysRepository.loadCcmjElectionKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadCcmjElectionKeysSQLException() throws SQLException {
		when(resultSet.next()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadCcmjElectionKeys(ELECTION_EVENT_ID));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadNodeKeys() throws SQLException, KeyManagementException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		NodeKeys nodeKeys = new NodeKeys.Builder().setCAKeys(privateKey, certificateChain).setEncryptionKeys(privateKey, certificateChain)
				.setLogSigningKeys(privateKey, certificateChain).setLogEncryptionKeys(privateKey, certificateChain).build();

		byte[] keysBytes = { 1, 2, 3 };
		when(resultSet.getBytes("keys")).thenReturn(keysBytes);
		when(codec.decodeNodeKeys(keysBytes, password)).thenReturn(nodeKeys);

		assertEquals(nodeKeys, keysRepository.loadNodeKeys(password));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
		verify(preparedStatement).setString(1, NODE_ID);
	}

	@Test
	void testLoadNodeKeysCodecException() throws SQLException, KeyManagementException {
		when(resultSet.getBytes("keys")).thenReturn(new byte[0]);
		when(codec.decodeNodeKeys(any(byte[].class), eq(password))).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadNodeKeys(password));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadNodeKeysNotFound() throws SQLException {
		when(resultSet.next()).thenReturn(false);

		assertThrows(KeyNotFoundException.class, () -> keysRepository.loadNodeKeys(password));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testLoadNodeKeysSQLException() throws SQLException {
		when(resultSet.next()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.loadNodeKeys(password));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveCcrjReturnCodesKeys() throws KeyManagementException, SQLException {
		byte[] generationSignature = { 1, 2, 3 };
		byte[] decryptionSignature = { 4, 5, 6 };
		CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys.Builder()
				.setCcrjReturnCodesGenerationKeys(elGamalPrivateKey, elGamalPublicKey, generationSignature)
				.setCcrjChoiceReturnCodesEncryptionKeys(elGamalPrivateKey, elGamalPublicKey, decryptionSignature).build();
		byte[] generationPrivateKeyBytes = { 1 };
		byte[] generationPublicKeyBytes = { 2 };
		byte[] decryptionPrivateKeyBytes = { 3 };
		byte[] decryptionPublicKeyBytes = { 4 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(generationPrivateKeyBytes, decryptionPrivateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(generationPublicKeyBytes, decryptionPublicKeyBytes);
		keysRepository.saveCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, ccrjReturnCodesKeys);

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		verify(preparedStatement, times(2)).setString(1, NODE_ID);
		verify(preparedStatement, times(2)).setString(2, ELECTION_EVENT_ID);
		verify(preparedStatement, times(2)).setString(3, VERIFICATION_CARD_SET_ID);
		verify(preparedStatement).setBytes(4, generationPrivateKeyBytes);
		verify(preparedStatement).setBytes(5, generationPublicKeyBytes);
		verify(preparedStatement).setBytes(6, generationSignature);
		verify(preparedStatement).setBytes(7, decryptionPrivateKeyBytes);
		verify(preparedStatement).setBytes(8, decryptionPublicKeyBytes);
		verify(preparedStatement).setBytes(9, decryptionSignature);
		verify(preparedStatement).executeUpdate();
	}

	@Test
	void testSaveCcrjReturnCodesKeysAlreadyExist() throws KeyManagementException, SQLException {
		byte[] generationSignature = { 1, 2, 3 };
		byte[] decryptionSignature = { 4, 5, 6 };
		CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys.Builder()
				.setCcrjReturnCodesGenerationKeys(elGamalPrivateKey, elGamalPublicKey, generationSignature)
				.setCcrjChoiceReturnCodesEncryptionKeys(elGamalPrivateKey, elGamalPublicKey, decryptionSignature).build();
		byte[] generationPrivateKeyBytes = { 1 };
		byte[] generationPublicKeyBytes = { 2 };
		byte[] decryptionPrivateKeyBytes = { 3 };
		byte[] decryptionPublicKeyBytes = { 4 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(generationPrivateKeyBytes, decryptionPrivateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(generationPublicKeyBytes, decryptionPublicKeyBytes);

		when(resultSet.getInt(1)).thenReturn(1);

		assertThrows(KeyAlreadyExistsException.class,
				() -> keysRepository.saveCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, ccrjReturnCodesKeys));
	}

	@Test
	void testSaveCcrjReturnCodesKeysCodecException() throws KeyManagementException {
		byte[] generationSignature = { 1, 2, 3 };
		byte[] decryptionSignature = { 4, 5, 6 };
		CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys.Builder()
				.setCcrjReturnCodesGenerationKeys(elGamalPrivateKey, elGamalPublicKey, generationSignature)
				.setCcrjChoiceReturnCodesEncryptionKeys(elGamalPrivateKey, elGamalPublicKey, decryptionSignature).build();

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenThrow(new KeyManagementException("test"));
		assertThrows(KeyManagementException.class, () -> keysRepository.saveCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, ccrjReturnCodesKeys));
	}

	@Test
	void testSaveCcrjReturnCodesKeysDuplicates() throws KeyManagementException, SQLException {
		byte[] generationSignature = { 1, 2, 3 };
		byte[] decryptionSignature = { 4, 5, 6 };
		CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys.Builder()
				.setCcrjReturnCodesGenerationKeys(elGamalPrivateKey, elGamalPublicKey, generationSignature)
				.setCcrjChoiceReturnCodesEncryptionKeys(elGamalPrivateKey, elGamalPublicKey, decryptionSignature).build();
		byte[] generationPrivateKeyBytes = { 1 };
		byte[] generationPublicKeyBytes = { 2 };
		byte[] decryptionPrivateKeyBytes = { 3 };
		byte[] decryptionPublicKeyBytes = { 4 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(generationPrivateKeyBytes, decryptionPrivateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(generationPublicKeyBytes, decryptionPublicKeyBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLIntegrityConstraintViolationException("test"));

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, ccrjReturnCodesKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveCcrjReturnCodesKeysSQLException() throws KeyManagementException, SQLException {
		byte[] generationSignature = { 1, 2, 3 };
		byte[] decryptionSignature = { 4, 5, 6 };
		CcrjReturnCodesKeys ccrjReturnCodesKeys = new CcrjReturnCodesKeys.Builder()
				.setCcrjReturnCodesGenerationKeys(elGamalPrivateKey, elGamalPublicKey, generationSignature)
				.setCcrjChoiceReturnCodesEncryptionKeys(elGamalPrivateKey, elGamalPublicKey, decryptionSignature).build();
		byte[] generationPrivateKeyBytes = { 1 };
		byte[] generationPublicKeyBytes = { 2 };
		byte[] decryptionPrivateKeyBytes = { 3 };
		byte[] decryptionPublicKeyBytes = { 4 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(generationPrivateKeyBytes, decryptionPrivateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(generationPublicKeyBytes, decryptionPublicKeyBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveCcrjReturnCodesKeys(ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, ccrjReturnCodesKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveElectionSigningKeys() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		byte[] passwordBytes = { 1 };
		byte[] keysBytes = { 2 };
		when(codec.encodePassword(password, encryptionPublicKey)).thenReturn(passwordBytes);
		when(codec.encodeElectionSigningKeys(electionSigningKeys, password)).thenReturn(keysBytes);

		keysRepository.saveElectionSigningKeys(ELECTION_EVENT_ID, electionSigningKeys);

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		verify(preparedStatement, times(2)).setString(1, NODE_ID);
		verify(preparedStatement, times(2)).setString(2, ELECTION_EVENT_ID);
		verify(preparedStatement).setBytes(3, keysBytes);
		verify(preparedStatement).setBytes(4, passwordBytes);
		verify(preparedStatement).executeUpdate();
		assertTrue(password.isDestroyed());
	}

	@Test
	void testSaveElectionSigningKeysAlreadyExists() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		byte[] passwordBytes = { 1 };
		byte[] keysBytes = { 2 };
		when(codec.encodePassword(password, encryptionPublicKey)).thenReturn(passwordBytes);
		when(codec.encodeElectionSigningKeys(electionSigningKeys, password)).thenReturn(keysBytes);
		when(resultSet.getInt(1)).thenReturn(1);

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveElectionSigningKeys(ELECTION_EVENT_ID, electionSigningKeys));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveElectionSigningKeysCodecException() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		byte[] passwordBytes = { 1 };
		when(codec.encodePassword(password, encryptionPublicKey)).thenReturn(passwordBytes);
		when(codec.encodeElectionSigningKeys(electionSigningKeys, password)).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveElectionSigningKeys(ELECTION_EVENT_ID, electionSigningKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		assertTrue(password.isDestroyed());
	}

	@Test
	void testSaveElectionSigningKeysDuplicates() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		byte[] passwordBytes = { 1 };
		byte[] keysBytes = { 2 };
		when(codec.encodePassword(password, encryptionPublicKey)).thenReturn(passwordBytes);
		when(codec.encodeElectionSigningKeys(electionSigningKeys, password)).thenReturn(keysBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLIntegrityConstraintViolationException("test"));

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveElectionSigningKeys(ELECTION_EVENT_ID, electionSigningKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		assertTrue(password.isDestroyed());
	}

	@Test
	void testSaveElectionSigningKeysGeneratorException() throws KeyManagementException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		when(generator.generatePassword()).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveElectionSigningKeys(ELECTION_EVENT_ID, electionSigningKeys));
	}

	@Test
	void testSaveElectionSigningKeysSQLException() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		ElectionSigningKeys electionSigningKeys = new ElectionSigningKeys(privateKey, certificateChain);

		byte[] passwordBytes = { 1 };
		byte[] keysBytes = { 2 };
		when(codec.encodePassword(password, encryptionPublicKey)).thenReturn(passwordBytes);
		when(codec.encodeElectionSigningKeys(electionSigningKeys, password)).thenReturn(keysBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveElectionSigningKeys(ELECTION_EVENT_ID, electionSigningKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		assertTrue(password.isDestroyed());
	}

	@Test
	void testSaveCcmjElectionKeys() throws KeyManagementException, SQLException {
		byte[] publicKeySignature = { 1, 2, 3 };
		CcmjElectionKeys ccmjElectionKeys = new CcmjElectionKeys(elGamalPrivateKey, elGamalPublicKey, publicKeySignature);
		byte[] privateKeyBytes = { 1 };
		byte[] publicKeyBytes = { 2 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(privateKeyBytes, privateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(publicKeyBytes, publicKeyBytes);

		keysRepository.saveCcmjElectionKeys(ELECTION_EVENT_ID, ccmjElectionKeys);

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		verify(preparedStatement, times(2)).setString(1, NODE_ID);
		verify(preparedStatement, times(2)).setString(2, ELECTION_EVENT_ID);
		verify(preparedStatement).setBytes(3, privateKeyBytes);
		verify(preparedStatement).setBytes(4, publicKeyBytes);
		verify(preparedStatement).setBytes(5, publicKeySignature);
		verify(preparedStatement).executeUpdate();
	}

	@Test
	void testSaveCcmjElectionKeysAlreadyExist() throws KeyManagementException, SQLException {
		byte[] publicKeySignature = { 1, 2, 3 };
		CcmjElectionKeys ccmjElectionKeys = new CcmjElectionKeys(elGamalPrivateKey, elGamalPublicKey, publicKeySignature);
		byte[] privateKeyBytes = { 1 };
		byte[] publicKeyBytes = { 2 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(privateKeyBytes, privateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(publicKeyBytes, publicKeyBytes);
		when(resultSet.getInt(1)).thenReturn(1);

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveCcmjElectionKeys(ELECTION_EVENT_ID, ccmjElectionKeys));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveCcmjElectionKeysCodecException() throws KeyManagementException, SQLException {
		byte[] publicKeySignature = { 1, 2, 3 };
		CcmjElectionKeys ccmjElectionKeys = new CcmjElectionKeys(elGamalPrivateKey, elGamalPublicKey, publicKeySignature);

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveCcmjElectionKeys(ELECTION_EVENT_ID, ccmjElectionKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveCcmjElectionKeysDuplicates() throws KeyManagementException, SQLException {
		byte[] publicKeySignature = { 1, 2, 3 };
		CcmjElectionKeys ccmjElectionKeys = new CcmjElectionKeys(elGamalPrivateKey, elGamalPublicKey, publicKeySignature);
		byte[] privateKeyBytes = { 1 };
		byte[] publicKeyBytes = { 2 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(privateKeyBytes, privateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(publicKeyBytes, publicKeyBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLIntegrityConstraintViolationException("test"));

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveCcmjElectionKeys(ELECTION_EVENT_ID, ccmjElectionKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveCcmjElectionKeysSQLException() throws KeyManagementException, SQLException {
		byte[] publicKeySignature = { 1, 2, 3 };
		CcmjElectionKeys ccmjElectionKeys = new CcmjElectionKeys(elGamalPrivateKey, elGamalPublicKey, publicKeySignature);
		byte[] privateKeyBytes = { 1 };
		byte[] publicKeyBytes = { 2 };

		when(codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionPublicKey)).thenReturn(privateKeyBytes, privateKeyBytes);
		when(codec.encodeElGamalPublicKey(elGamalPublicKey)).thenReturn(publicKeyBytes, publicKeyBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveCcmjElectionKeys(ELECTION_EVENT_ID, ccmjElectionKeys));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveNodeKeys() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		NodeKeys nodeKeys = new NodeKeys.Builder().setCAKeys(privateKey, certificateChain).setEncryptionKeys(privateKey, certificateChain)
				.setLogSigningKeys(privateKey, certificateChain).setLogEncryptionKeys(privateKey, certificateChain).build();

		byte[] keysBytes = { 1, 2, 3 };

		when(codec.encodeNodeKeys(nodeKeys, password)).thenReturn(keysBytes);

		keysRepository.saveNodeKeys(nodeKeys, password);

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
		verify(preparedStatement, times(2)).setString(1, NODE_ID);
		verify(preparedStatement).setBytes(2, keysBytes);
		verify(preparedStatement).executeUpdate();
	}

	@Test
	void testSaveNodeKeysAlreadyExist() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		NodeKeys nodeKeys = new NodeKeys.Builder().setCAKeys(privateKey, certificateChain).setEncryptionKeys(privateKey, certificateChain)
				.setLogSigningKeys(privateKey, certificateChain).setLogEncryptionKeys(privateKey, certificateChain).build();

		byte[] keysBytes = { 1, 2, 3 };

		when(codec.encodeNodeKeys(nodeKeys, password)).thenReturn(keysBytes);
		when(resultSet.getInt(1)).thenReturn(1);

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveNodeKeys(nodeKeys, password));

		verify(connection).close();
		verify(preparedStatement).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveNodeKeysCodecException() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		NodeKeys nodeKeys = new NodeKeys.Builder().setCAKeys(privateKey, certificateChain).setEncryptionKeys(privateKey, certificateChain)
				.setLogSigningKeys(privateKey, certificateChain).setLogEncryptionKeys(privateKey, certificateChain).build();

		when(codec.encodeNodeKeys(nodeKeys, password)).thenThrow(new KeyManagementException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveNodeKeys(nodeKeys, password));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveNodeKeysDuplicates() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		NodeKeys nodeKeys = new NodeKeys.Builder().setCAKeys(privateKey, certificateChain).setEncryptionKeys(privateKey, certificateChain)
				.setLogSigningKeys(privateKey, certificateChain).setLogEncryptionKeys(privateKey, certificateChain).build();

		byte[] keysBytes = { 1, 2, 3 };

		when(codec.encodeNodeKeys(nodeKeys, password)).thenReturn(keysBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLIntegrityConstraintViolationException("test"));

		assertThrows(KeyAlreadyExistsException.class, () -> keysRepository.saveNodeKeys(nodeKeys, password));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

	@Test
	void testSaveNodeKeysSQLException() throws KeyManagementException, SQLException {
		PrivateKey privateKey = mock(PrivateKey.class);
		X509Certificate[] certificateChain = { mock(X509Certificate.class) };
		NodeKeys nodeKeys = new NodeKeys.Builder().setCAKeys(privateKey, certificateChain).setEncryptionKeys(privateKey, certificateChain)
				.setLogSigningKeys(privateKey, certificateChain).setLogEncryptionKeys(privateKey, certificateChain).build();

		byte[] keysBytes = { 1, 2, 3 };

		when(codec.encodeNodeKeys(nodeKeys, password)).thenReturn(keysBytes);
		when(preparedStatement.executeUpdate()).thenThrow(new SQLException("test"));

		assertThrows(KeyManagementException.class, () -> keysRepository.saveNodeKeys(nodeKeys, password));

		verify(connection).close();
		verify(preparedStatement, times(2)).close();
		verify(resultSet).close();
	}

}
