/*
 * (c) Copyright 2020 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain.exception;

public class CombinedCorrectnessInformationRepositoryException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public CombinedCorrectnessInformationRepositoryException(String message, Throwable cause) {
		super(message, cause);
	}

	public CombinedCorrectnessInformationRepositoryException(String message) {
		super(message);
	}

}
