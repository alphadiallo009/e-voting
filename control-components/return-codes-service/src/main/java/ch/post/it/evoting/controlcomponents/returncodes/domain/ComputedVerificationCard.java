/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.UUIDValidations.validateUUID;


public class ComputedVerificationCard {

	private String electionEventId;
	private String verificationCardId;
	private Integer confirmationAttempts = 0;
	private boolean exponentiationComputed = false;

	public ComputedVerificationCard(final String electionEventId, final String verificationCardId) {
		validateUUID(electionEventId);
		validateUUID(verificationCardId);

		this.electionEventId = electionEventId;
		this.verificationCardId = verificationCardId;
	}

	public String getElectionEventId() {
		return electionEventId;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public Integer getConfirmationAttempts() {
		return this.confirmationAttempts;
	}

	public void setConfirmationAttempts(final int confirmationAttempts) {
		this.confirmationAttempts = confirmationAttempts;
	}

	public boolean isExponentiationComputed() {
		return this.exponentiationComputed;
	}

	public void setExponentiationComputed(final boolean exponentiationComputed) {
		this.exponentiationComputed = exponentiationComputed;
	}

	@Override
	public String toString() {
		return String
				.format("ComputedVerificationCard{electionEventId='%s', verificationCardId='%s', confirmationAttempts=%s, isExponentiationComputed=%s}'",
						this.electionEventId, this.verificationCardId, this.confirmationAttempts, this.exponentiationComputed);
	}
}
