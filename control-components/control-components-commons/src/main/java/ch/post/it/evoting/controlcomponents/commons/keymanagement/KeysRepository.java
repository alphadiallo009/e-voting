/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.commons.keymanagement;

import java.security.KeyManagementException;
import java.security.KeyPair;
import java.security.KeyStore.PasswordProtection;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.concurrent.atomic.AtomicReference;

import javax.security.auth.DestroyFailedException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import ch.post.it.evoting.controlcomponents.commons.keymanagement.exception.KeyAlreadyExistsException;
import ch.post.it.evoting.controlcomponents.commons.keymanagement.exception.KeyNotFoundException;
import ch.post.it.evoting.controlcomponents.commons.keymanagement.exception.SavingKeyException;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalPrivateKey;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalPublicKey;

@Repository
public class KeysRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(KeysRepository.class);

	private final DataSource dataSource;

	private final Codec codec;

	private final Generator generator;

	private final String nodeId;

	private AtomicReference<KeyPair> encryptionKeys;

	public KeysRepository(final DataSource dataSource,
			final Codec codec,
			final Generator generator,
			@Value("${key.node.id}")
			final String nodeId) {
		this.dataSource = dataSource;
		this.codec = codec;
		this.generator = generator;
		this.nodeId = nodeId;
	}

	public CcrjReturnCodesKeys loadCcrjReturnCodesKeys(final String electionEventId, final String verificationCardSetId)
			throws KeyManagementException {

		final String sql = "SELECT CCRJ_RETURN_CODES_GENERATION_SECRET_KEY, CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY, "
				+ "CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY_SIGNATURE, CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_SECRET_KEY,"
				+ " CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY, CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY_SIGNATURE "
				+ "FROM CCR_RETURN_CODES_KEYS "
				+ "WHERE NODE_ID = ? AND ELECTION_EVENT_ID = ? AND VERIFICATION_CARD_SET_ID = ?";

		try (final Connection connection = dataSource.getConnection(); final PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			statement.setString(3, verificationCardSetId);

			try (final ResultSet resultSet = statement.executeQuery()) {
				if (!resultSet.next()) {
					throw new KeyNotFoundException("CCR_j Return Codes keys not found.");
				}

				final CcrjReturnCodesKeys.Builder builder = new CcrjReturnCodesKeys.Builder();
				builder.setCcrjReturnCodesGenerationKeys(
						getElGamalPrivateKey(resultSet, "CCRJ_RETURN_CODES_GENERATION_SECRET_KEY"),
						getElGamalPublicKey(resultSet, "CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY"),
						resultSet.getBytes("CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY_SIGNATURE"));

				builder.setCcrjChoiceReturnCodesEncryptionKeys(
						getElGamalPrivateKey(resultSet, "CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_SECRET_KEY"),
						getElGamalPublicKey(resultSet, "CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY"),
						resultSet.getBytes("CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY_SIGNATURE"));
				return builder.build();
			}
		} catch (SQLException e) {
			throw new KeyManagementException("Failed to load CCR_j Return Codes keys.", e);
		}
	}

	public ElectionSigningKeys loadElectionSigningKeys(String electionEventId) throws KeyManagementException {
		String sql = "select keys, password from cc_election_signing_keys where node_id = ? and election_event_id = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (!resultSet.next()) {
					throw new KeyNotFoundException("Election signing keys not found.");
				}
				PasswordProtection password = getPassword(resultSet, "password");
				try {
					return getElectionSigningKeys(resultSet, "keys", password);
				} finally {
					destroyPassword(electionEventId, password);
				}
			}
		} catch (SQLException e) {
			throw new KeyManagementException("Failed to load election signing keys.", e);
		}
	}

	private void destroyPassword(String electionEventId, PasswordProtection password) {
		try {
			password.destroy();
		} catch (DestroyFailedException e) {
			LOGGER.warn(String.format("Failed to destroy the password for node id %s and election event id %s.", nodeId, electionEventId), e);
		}
	}

	public CcmjElectionKeys loadCcmjElectionKeys(final String electionEventId) throws KeyManagementException {
		final String sql = "SELECT CCMJ_ELECTION_SECRET_KEY, CCMJ_ELECTION_PUBLIC_KEY, CCMJ_ELECTION_PUBLIC_KEY_SIGNATURE "
				+ "FROM CCM_ELECTION_KEY "
				+ "WHERE NODE_ID = ? AND ELECTION_EVENT_ID = ?";

		try (final Connection connection = dataSource.getConnection(); final PreparedStatement statement = connection.prepareStatement(sql)) {

			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);

			try (final ResultSet resultSet = statement.executeQuery()) {

				if (!resultSet.next()) {
					throw new KeyNotFoundException("CCM_j Election keys not found.");
				}

				return new CcmjElectionKeys(
						getElGamalPrivateKey(resultSet, "CCMJ_ELECTION_SECRET_KEY"),
						getElGamalPublicKey(resultSet, "CCMJ_ELECTION_PUBLIC_KEY"),
						resultSet.getBytes("CCMJ_ELECTION_PUBLIC_KEY_SIGNATURE"));
			}

		} catch (SQLException e) {
			throw new KeyManagementException("Failed to load CCM_j election keys.", e);
		}
	}

	public NodeKeys loadNodeKeys(PasswordProtection password) throws KeyManagementException {
		String sql = "select keys from cc_node_keys where node_id = ?";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (!resultSet.next()) {
					throw new KeyNotFoundException("Node keys not found.");
				}
				return getNodeKeys(resultSet, "keys", password);
			}
		} catch (SQLException e) {
			throw new KeyManagementException("Failed to load node keys.", e);
		}
	}

	public void saveCcrjReturnCodesKeys(final String electionEventId, final String verificationCardSetId,
			final CcrjReturnCodesKeys ccrjReturnCodesKeys)
			throws KeyManagementException {
		try (final Connection connection = dataSource.getConnection()) {
			if (hasCcrjReturnCodesKeys(connection, electionEventId, verificationCardSetId)) {
				throw new KeyAlreadyExistsException("CCR_j Return Codes Keys already exist.");
			}
			insertCcrjReturnCodesKeys(connection, electionEventId, verificationCardSetId, ccrjReturnCodesKeys);
		} catch (SQLException e) {
			throw new KeyManagementException("Failed to save CCR_j Return Codes keys.", e);
		}
	}

	public void saveElectionSigningKeys(String electionEventId, ElectionSigningKeys electionSigningKeys) throws KeyManagementException {
		try (Connection connection = dataSource.getConnection()) {
			checkElectionSigningKeysDoNotExist(connection, electionEventId);
			PasswordProtection password = generator.generatePassword();
			try {
				insertElectionSigningKeys(connection, electionEventId, electionSigningKeys, password);
			} finally {
				destroyPassword(electionEventId, password);
			}
		} catch (SQLException e) {
			throw new KeyManagementException("Failed to save election signing keys.", e);
		}
	}

	public void saveCcmjElectionKeys(final String electionEventId, final CcmjElectionKeys ccmjElectionKeys)
			throws KeyManagementException {
		try (final Connection connection = dataSource.getConnection()) {
			if (hasCcmjElectionKeys(connection, electionEventId)) {
				throw new KeyAlreadyExistsException("CCM_j election keys already exist.");
			}
			insertCcmjElectionKeys(connection, electionEventId, ccmjElectionKeys);
		} catch (SQLException e) {
			throw new KeyManagementException("Failed to save CCM_j election keys.", e);
		}
	}

	public void saveNodeKeys(NodeKeys nodeKeys, PasswordProtection password) throws KeyManagementException {
		try (Connection connection = dataSource.getConnection()) {
			checkNodeKeysDoNotExist(connection);
			insertNodeKeys(connection, nodeKeys, password);
		} catch (SQLException e) {
			throw new SavingKeyException("Failed to save node keys.", e);
		}
	}

	public void setEncryptionKeys(final PrivateKey privateKey, final PublicKey publicKey) {
		this.encryptionKeys = new AtomicReference<>(new KeyPair(publicKey, privateKey));
	}

	public boolean hasCcrjReturnCodesKeys(final String electionEventId, final String verificationCardSetId) throws KeyManagementException {
		try (final Connection connection = dataSource.getConnection()) {
			return hasCcrjReturnCodesKeys(connection, electionEventId, verificationCardSetId);
		} catch (SQLException e) {
			throw new KeyManagementException(e);
		}
	}

	private boolean hasCcrjReturnCodesKeys(final Connection connection, final String electionEventId, final String verificationCardSetId)
			throws SQLException {

		final String sql = "SELECT COUNT(*) FROM CCR_RETURN_CODES_KEYS WHERE NODE_ID = ? AND ELECTION_EVENT_ID = ? AND VERIFICATION_CARD_SET_ID = ?";

		try (final PreparedStatement statement = connection.prepareStatement(sql)) {

			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			statement.setString(3, verificationCardSetId);

			try (final ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next() && resultSet.getInt(1) > 0) {
					return true;
				}
			}
		}
		return false;
	}

	private void checkElectionSigningKeysDoNotExist(Connection connection, String electionEventId) throws KeyAlreadyExistsException, SQLException {
		String sql = "select count(*) from cc_election_signing_keys where node_id = ? and election_event_id = ?";
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next() && resultSet.getInt(1) > 0) {
					throw new KeyAlreadyExistsException("Election signing keys already exist.");
				}
			}
		}
	}

	public boolean hasCcmjElectionKeys(final String electionEventId) throws KeyManagementException {
		try (final Connection connection = dataSource.getConnection()) {
			return hasCcmjElectionKeys(connection, electionEventId);
		} catch (SQLException e) {
			throw new KeyManagementException(e);
		}
	}

	private boolean hasCcmjElectionKeys(final Connection connection, final String electionEventId) throws SQLException {

		final String sql = "SELECT COUNT(*) FROM CCM_ELECTION_KEY WHERE NODE_ID = ? AND ELECTION_EVENT_ID = ?";

		try (final PreparedStatement statement = connection.prepareStatement(sql)) {

			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);

			try (final ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next() && resultSet.getInt(1) > 0) {
					return true;
				}
			}
		}

		return false;
	}

	private void checkNodeKeysDoNotExist(Connection connection) throws KeyAlreadyExistsException, SQLException {
		String sql = "select count(*) from cc_node_keys where node_id = ?";
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			try (ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next() && resultSet.getInt(1) > 0) {
					throw new KeyAlreadyExistsException("Node keys already exist.");
				}
			}
		}
	}

	private ElectionSigningKeys getElectionSigningKeys(ResultSet resultSet, String columnLabel, PasswordProtection password)
			throws KeyManagementException, SQLException {
		byte[] bytes = resultSet.getBytes(columnLabel);
		return codec.decodeElectionSigningKeys(bytes, password);
	}

	private ElGamalPrivateKey getElGamalPrivateKey(ResultSet resultSet, String columnLabel) throws KeyManagementException, SQLException {
		byte[] bytes = resultSet.getBytes(columnLabel);
		return codec.decodeElGamalPrivateKey(bytes, encryptionKeys.get().getPrivate());
	}

	private ElGamalPublicKey getElGamalPublicKey(ResultSet resultSet, String columnLabel) throws KeyManagementException, SQLException {
		byte[] bytes = resultSet.getBytes(columnLabel);
		return codec.decodeElGamalPublicKey(bytes);
	}

	private NodeKeys getNodeKeys(ResultSet resultSet, String columnLabel, PasswordProtection password) throws KeyManagementException, SQLException {
		byte[] bytes = resultSet.getBytes(columnLabel);
		return codec.decodeNodeKeys(bytes, password);
	}

	private PasswordProtection getPassword(ResultSet resultSet, String columnLabel) throws KeyManagementException, SQLException {
		byte[] bytes = resultSet.getBytes(columnLabel);
		return codec.decodePassword(bytes, encryptionKeys.get().getPrivate());
	}

	private void insertCcrjReturnCodesKeys(final Connection connection, final String electionEventId, final String verificationCardSetId,
			final CcrjReturnCodesKeys ccrjReturnCodesKeys) throws KeyManagementException, SQLException {

		String sql = "INSERT INTO CCR_RETURN_CODES_KEYS (NODE_ID, ELECTION_EVENT_ID, VERIFICATION_CARD_SET_ID, "
				+ "CCRJ_RETURN_CODES_GENERATION_SECRET_KEY, CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY, "
				+ "CCRJ_RETURN_CODES_GENERATION_PUBLIC_KEY_SIGNATURE, CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_SECRET_KEY, "
				+ "CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY, CCRJ_CHOICE_RETURN_CODES_ENCRYPTION_PUBLIC_KEY_SIGNATURE) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			statement.setString(3, verificationCardSetId);

			setElGamalPrivateKey(statement, 4, ccrjReturnCodesKeys.getCcrjReturnCodesGenerationSecretKey());
			setElGamalPublicKey(statement, 5, ccrjReturnCodesKeys.getCcrjReturnCodesGenerationPublicKey());
			statement.setBytes(6, ccrjReturnCodesKeys.getCcrjReturnCodesGenerationPublicKeySignature());

			setElGamalPrivateKey(statement, 7, ccrjReturnCodesKeys.getCcrjChoiceReturnCodesEncryptionSecretKey());
			setElGamalPublicKey(statement, 8, ccrjReturnCodesKeys.getCcrjChoiceReturnCodesEncryptionPublicKey());
			statement.setBytes(9, ccrjReturnCodesKeys.getCcrjChoiceReturnCodesEncryptionPublicKeySignature());

			statement.executeUpdate();

		} catch (SQLIntegrityConstraintViolationException e) {
			throw new KeyAlreadyExistsException("CCR_j Return codes keys already exist.", e);
		}
	}

	private void insertElectionSigningKeys(Connection connection, String electionEventId, ElectionSigningKeys electionSigningKeys,
			PasswordProtection password) throws KeyManagementException, SQLException {
		String sql = "insert into cc_election_signing_keys (node_id, election_event_id, keys, password) values (?, ?, ?, ?)";
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			setElectionSigningKeys(statement, 3, electionSigningKeys, password);
			setPassword(statement, 4, password);
			statement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new KeyAlreadyExistsException("Election signing keys already exist.", e);
		}
	}

	private void insertCcmjElectionKeys(final Connection connection, final String electionEventId, final CcmjElectionKeys ccmjElectionKeys)
			throws KeyManagementException, SQLException {
		final String sql = "INSERT INTO CCM_ELECTION_KEY (NODE_ID, ELECTION_EVENT_ID, CCMJ_ELECTION_SECRET_KEY , CCMJ_ELECTION_PUBLIC_KEY , CCMJ_ELECTION_PUBLIC_KEY_SIGNATURE ) VALUES (?, ?, ?, ?, ?)";

		try (final PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			statement.setString(2, electionEventId);
			setElGamalPrivateKey(statement, 3, ccmjElectionKeys.getCcmjElectionSecretKey());
			setElGamalPublicKey(statement, 4, ccmjElectionKeys.getCcmjElectionPublicKey());
			statement.setBytes(5, ccmjElectionKeys.getCcmjElectionPublicKeySignature());

			statement.executeUpdate();

		} catch (SQLIntegrityConstraintViolationException e) {
			throw new KeyAlreadyExistsException("CCM_j election keys already exist.", e);
		}
	}

	private void insertNodeKeys(Connection connection, NodeKeys nodeKeys, PasswordProtection password) throws KeyManagementException, SQLException {
		String sql = "insert into cc_node_keys (node_id, keys) values (?, ?)";
		try (PreparedStatement statement = connection.prepareStatement(sql)) {
			statement.setString(1, nodeId);
			setNodeKeys(statement, 2, nodeKeys, password);
			statement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new KeyAlreadyExistsException("Node keys already exist.", e);
		}
	}

	private void setElectionSigningKeys(final PreparedStatement preparedStatement, final int index, final ElectionSigningKeys electionSigningKeys,
			final PasswordProtection password)
			throws KeyManagementException, SQLException {
		final byte[] bytes = codec.encodeElectionSigningKeys(electionSigningKeys, password);
		preparedStatement.setBytes(index, bytes);
	}

	private void setElGamalPrivateKey(final PreparedStatement preparedStatement, final int index, final ElGamalPrivateKey elGamalPrivateKey)
			throws KeyManagementException, SQLException {
		final byte[] bytes = codec.encodeElGamalPrivateKey(elGamalPrivateKey, encryptionKeys.get().getPublic());
		preparedStatement.setBytes(index, bytes);
	}

	private void setElGamalPublicKey(final PreparedStatement preparedStatement, final int index, final ElGamalPublicKey elGamalPublicKey)
			throws KeyManagementException, SQLException {
		final byte[] bytes = codec.encodeElGamalPublicKey(elGamalPublicKey);
		preparedStatement.setBytes(index, bytes);
	}

	private void setNodeKeys(final PreparedStatement preparedStatement, final int index, final NodeKeys nodeKeys,
			final PasswordProtection passwordProtection) throws KeyManagementException, SQLException {
		final byte[] bytes = codec.encodeNodeKeys(nodeKeys, passwordProtection);
		preparedStatement.setBytes(index, bytes);
	}

	private void setPassword(final PreparedStatement preparedStatement, final int index, final PasswordProtection passwordProtection)
			throws KeyManagementException, SQLException {
		final byte[] bytes = codec.encodePassword(passwordProtection, encryptionKeys.get().getPublic());
		preparedStatement.setBytes(index, bytes);
	}

}
