/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.certificates.utils.CryptographicOperationException;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.ZpGroupElement;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.ZpSubgroup;
import ch.post.it.evoting.cryptoprimitives.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.domain.election.EncryptionParameters;
import ch.post.it.evoting.domain.election.VoteVerificationContextData;
import ch.post.it.evoting.domain.election.model.vote.Vote;
import ch.post.it.evoting.domain.returncodes.ChoiceCodesVerificationDecryptResPayload;
import ch.post.it.evoting.domain.returncodes.ComputeResults;
import ch.post.it.evoting.domain.returncodes.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.PartialChoiceReturnCodesVerificationInput;
import ch.post.it.evoting.domain.returncodes.ReturnCodesInput;
import ch.post.it.evoting.domain.returncodes.ShortChoiceReturnCodeAndComputeResults;
import ch.post.it.evoting.domain.returncodes.VoteAndComputeResults;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.choicecode.CodesDecryptionResults;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.ElectionPublicKey;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.ElectionPublicKeyRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.VerificationContent;
import ch.post.it.evoting.votingserver.voteverification.domain.model.content.VerificationContentRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.verification.Verification;
import ch.post.it.evoting.votingserver.voteverification.domain.model.verification.VerificationRepository;
import ch.post.it.evoting.votingserver.voteverification.domain.model.verificationset.VerificationSetEntity;
import ch.post.it.evoting.votingserver.voteverification.domain.model.verificationset.VerificationSetRepository;

/**
 * Generate the short choice return codes based on the encrypted partial choice return codes - in interaction with the control components.
 */
@Stateless
public class ChoiceCodesServiceImpl implements ChoiceCodesService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceCodesServiceImpl.class);

	@Inject
	private VerificationSetRepository verificationSetRepository;

	@Inject
	private ElectionPublicKeyRepository electionPublicKeyRepository;

	@Inject
	private VerificationContentRepository verificationContentRepository;

	@Inject
	private VerificationRepository verificationRepository;

	@Inject
	private CodesDecrypterService codesDecrypterService;

	@Inject
	private CodesComputeService codesComputeService;

	@Inject
	private ExtractCRCService extractCRCService;

	@Inject
	private ObjectMapper mapper;

	@Override
	public ShortChoiceReturnCodeAndComputeResults generateShortChoiceReturnCodes(final String tenantId, final String electionEventId,
			final String verificationCardId, final VoteAndComputeResults voteAndComputeResults)
			throws ResourceNotFoundException, GeneralCryptoLibException, IOException, CryptographicOperationException {

		LOGGER.info("Generating short Choice Return Codes for tenant: {}, election event: {} and verification card: {}.", tenantId, electionEventId,
				verificationCardId);

		final Vote vote = voteAndComputeResults.getVote();
		final ZpSubgroup zpSubgroup = getZpSubgroupFromVote(vote);

		final String[] encryptedPartialChoiceReturnCodesAsString = vote.getEncryptedPartialChoiceCodes().split(";");
		final List<BigInteger> encryptedPartialChoiceReturnCodes = new ArrayList<>();
		final List<ZpGroupElement> encryptedPartialChoiceReturnCodesAsElements = new ArrayList<>();
		for (final String encryptedPcc : encryptedPartialChoiceReturnCodesAsString) {
			final BigInteger encryptedPartialChoiceReturnCodeAsBigInteger = new BigInteger(encryptedPcc);
			encryptedPartialChoiceReturnCodes.add(encryptedPartialChoiceReturnCodeAsBigInteger);
			encryptedPartialChoiceReturnCodesAsElements.add(new ZpGroupElement(encryptedPartialChoiceReturnCodeAsBigInteger, zpSubgroup));
		}

		final ReturnCodesInput encryptedPartialChoiceReturnCodesInput = createVerificationPayload(
				Collections.singletonList(encryptedPartialChoiceReturnCodes.get(0)), vote);
		encryptedPartialChoiceReturnCodesInput.setCertificates(voteAndComputeResults.getCredentialInfoCertificates());

		final Verification verification = verificationRepository.findByTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId,
				verificationCardId);
		final String verificationCardSetId = verification.getVerificationCardSetId();

		final ComputeResults computeAndDecryptResults = voteAndComputeResults.getComputeResults();

		final CodesDecryptionResults decryptionResult;
		if (computeAndDecryptResults == null || computeAndDecryptResults.getDecryptionResults() == null || computeAndDecryptResults
				.getDecryptionResults().isEmpty()) {

			decryptionResult = codesDecrypterService
					.decryptPartialCodes(tenantId, electionEventId, verificationCardSetId, verificationCardId,
							encryptedPartialChoiceReturnCodesAsElements,
							encryptedPartialChoiceReturnCodesInput);

		} else {
			final String decyptionResultsString = computeAndDecryptResults.getDecryptionResults();
			final List<ChoiceCodesVerificationDecryptResPayload> ccnContributions = mapper
					.readValue(decyptionResultsString, new TypeReference<List<ChoiceCodesVerificationDecryptResPayload>>() {
					});
			decryptionResult = codesDecrypterService.decryptPartialCodesFromDecryptResult(encryptedPartialChoiceReturnCodesAsElements,
					ccnContributions);
		}

		final List<BigInteger> toComputePartialCodesList = decryptionResult.getCombinedZpGroupElementLists().stream().map(ZpGroupElement::getValue)
				.collect(Collectors.toList());

		final ReturnCodesInput toComputeReturnCodesInput = new ReturnCodesInput();
		toComputeReturnCodesInput.setReturnCodesInputElements(toComputePartialCodesList);

		// Determine if the long choice return codes have been previously computed. This happens in the case of a re-login.
		final List<LongChoiceReturnCodesShare> computationResults;
		if (computeAndDecryptResults == null || computeAndDecryptResults.getComputationResults() == null || computeAndDecryptResults
				.getComputationResults().isEmpty()) {

			// Ask the control components to compute the long choice return codes.
			computationResults = codesComputeService
					.computeLongChoiceReturnCodeShares(tenantId, electionEventId, verificationCardSetId, verificationCardId,
							toComputeReturnCodesInput);
			LOGGER.info("Successfully retrieved the long choice return codes shares for card {}-{}-{}.", electionEventId,
					verificationCardSetId, verificationCardId);
		} else {
			// The long return choice codes have been previously computed.
			final String computationResultsString = computeAndDecryptResults.getComputationResults();
			computationResults = mapper.readValue(computationResultsString, new TypeReference<List<LongChoiceReturnCodesShare>>() {
			});
		}

		// Retrieve short codes by combining CCR shares and looking up the CMTable.
		final List<GroupVector<GqElement, GqGroup>> lCCShares = computationResults.stream()
				.map(LongChoiceReturnCodesShare::getLongChoiceReturnCodeShare)
				.collect(Collectors.toList());
		final List<String> shortCodes = extractCRCService.extractCRC(lCCShares, verificationCardId, vote);

		// Prepare response.
		final ShortChoiceReturnCodeAndComputeResults choiceCodes = new ShortChoiceReturnCodeAndComputeResults();

		final String serializedShortCodes = StringUtils.join(shortCodes, ';');
		choiceCodes.setShortChoiceReturnCodes(serializedShortCodes);

		try {
			choiceCodes.setComputationResults(mapper.writeValueAsString(computationResults));
			choiceCodes.setDecryptionResults(mapper.writeValueAsString(decryptionResult.getDecryptResult()));
		} catch (JsonProcessingException e) {
			throw new IllegalStateException("Could not serialize choice codes computation results", e);
		}

		return choiceCodes;
	}

	private ZpSubgroup getZpSubgroupFromVote(final Vote vote) throws ResourceNotFoundException, GeneralCryptoLibException, IOException {
		final VerificationContent verificationContent;

		try {
			Verification verification = verificationRepository
					.findByTenantIdElectionEventIdVerificationCardId(vote.getTenantId(), vote.getElectionEventId(), vote.getVerificationCardId());
			verificationContent = verificationContentRepository
					.findByTenantIdElectionEventIdVerificationCardSetId(vote.getTenantId(), vote.getElectionEventId(),
							verification.getVerificationCardSetId());
		} catch (ResourceNotFoundException e) {
			LOGGER.error("Error creating ZpSubgroup from Vote.");
			throw e;
		}

		final ZpSubgroup mathematicalGroup;
		try {
			final VoteVerificationContextData voteVerificationContextData = mapper.readValue(verificationContent.getJson(),
					VoteVerificationContextData.class);
			final EncryptionParameters encryptionParameters = voteVerificationContextData.getEncryptionParameters();
			final String generatorEncryptParam = encryptionParameters.getG();
			final String pEncryptParam = encryptionParameters.getP();
			final String qEncryptParam = encryptionParameters.getQ();

			final BigInteger generatorEncryptParamBigInteger = new BigInteger(generatorEncryptParam);
			final BigInteger pEncryptParamBigInteger = new BigInteger(pEncryptParam);
			final BigInteger qEncryptParamBigInteger = new BigInteger(qEncryptParam);
			mathematicalGroup = new ZpSubgroup(generatorEncryptParamBigInteger, pEncryptParamBigInteger, qEncryptParamBigInteger);
		} catch (GeneralCryptoLibException | NumberFormatException | IOException e) {
			LOGGER.error("Error creating ZpSubgroup from Vote.");
			throw e;
		}

		return mathematicalGroup;
	}

	private ReturnCodesInput createVerificationPayload(final List<BigInteger> encryptedPartialChoiceReturnCodes, final Vote vote)
			throws ResourceNotFoundException {

		try {
			final Verification verification = verificationRepository
					.findByTenantIdElectionEventIdVerificationCardId(vote.getTenantId(), vote.getElectionEventId(), vote.getVerificationCardId());

			final VerificationContent verificationContent = verificationContentRepository
					.findByTenantIdElectionEventIdVerificationCardSetId(vote.getTenantId(), vote.getElectionEventId(),
							verification.getVerificationCardSetId());

			final VerificationSetEntity verificationSetEntity = verificationSetRepository
					.findByTenantIdElectionEventIdVerificationCardSetId(vote.getTenantId(), vote.getElectionEventId(),
							verification.getVerificationCardSetId());

			final VoteVerificationContextData voteVerificationContextData = mapper.readValue(verificationContent.getJson(),
					VoteVerificationContextData.class);

			final String electoralAuthorityId = voteVerificationContextData.getElectoralAuthorityId();

			final ElectionPublicKey electionPublicKey = electionPublicKeyRepository
					.findByTenantIdElectionEventIdElectoralAuthorityId(vote.getTenantId(), vote.getElectionEventId(), electoralAuthorityId);

			final PartialChoiceReturnCodesVerificationInput partialChoiceReturnCodesVerificationInput = new PartialChoiceReturnCodesVerificationInput();
			partialChoiceReturnCodesVerificationInput.setVote(mapper.writeValueAsString(vote));
			partialChoiceReturnCodesVerificationInput.setElectionPublicKeyJwt(electionPublicKey.getJwt());
			partialChoiceReturnCodesVerificationInput.setVerificationCardSetDataJwt(verificationSetEntity.getSignature());

			final ReturnCodesInput returnCodesInput = new ReturnCodesInput();
			returnCodesInput.setPartialChoiceReturnCodesVerificationInput(partialChoiceReturnCodesVerificationInput);
			returnCodesInput.setReturnCodesInputElements(encryptedPartialChoiceReturnCodes);

			return returnCodesInput;
		} catch (IOException e) {
			throw new ResourceNotFoundException(e.getMessage(), e);
		}

	}
}
