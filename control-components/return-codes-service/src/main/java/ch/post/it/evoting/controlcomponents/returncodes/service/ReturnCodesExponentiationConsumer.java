/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.service;

import static ch.post.it.evoting.controlcomponents.returncodes.service.CreateLCCShareService.CreateLCCShareOutput;
import static ch.post.it.evoting.controlcomponents.returncodes.service.CreateLVCCShareService.CreateLVCCShareOutput;
import static ch.post.it.evoting.cryptoprimitives.domain.validations.UUIDValidations.validateUUID;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.util.Base64;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponents.commons.keymanagement.KeysManager;
import ch.post.it.evoting.controlcomponents.commons.payloadsignature.CryptolibPayloadSignatureService;
import ch.post.it.evoting.controlcomponents.returncodes.domain.CombinedCorrectnessInformationRepository;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ControlComponentContext;
import ch.post.it.evoting.controlcomponents.returncodes.domain.LongChoiceReturnCodesShareExponentiationProof;
import ch.post.it.evoting.controlcomponents.returncodes.domain.LongVoteCastReturnCodesShareExponentiationProof;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ReturnCodesMessage;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ReturnCodesMessageFactory;
import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.ComputedVerificationCardNotFoundException;
import ch.post.it.evoting.cryptolib.elgamal.bean.ElGamalPrivateKey;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.ZpSubgroup;
import ch.post.it.evoting.cryptoprimitives.GroupVector;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.mapper.DomainObjectMapper;
import ch.post.it.evoting.cryptoprimitives.domain.signature.CryptoPrimitivesPayloadSignature;
import ch.post.it.evoting.cryptoprimitives.hashing.HashService;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.domain.cryptoadapters.CryptoAdapters;
import ch.post.it.evoting.domain.election.model.confirmation.TraceableConfirmationMessage;
import ch.post.it.evoting.domain.election.model.messaging.PayloadSignatureException;
import ch.post.it.evoting.domain.election.model.messaging.SafeStreamDeserializationException;
import ch.post.it.evoting.domain.returncodes.LongChoiceReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.LongReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.LongReturnCodesSharePayload;
import ch.post.it.evoting.domain.returncodes.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.ReturnCodeComputationDTO;
import ch.post.it.evoting.domain.returncodes.ReturnCodesInput;
import ch.post.it.evoting.domain.returncodes.safestream.StreamSerializableObjectReader;
import ch.post.it.evoting.domain.returncodes.safestream.StreamSerializableObjectReaderImpl;

@Service
public class ReturnCodesExponentiationConsumer {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnCodesExponentiationConsumer.class);
	private static final org.apache.logging.log4j.Logger SECURE_LOGGER = LogManager.getLogger("SecureLog");

	private final RabbitTemplate rabbitTemplate;
	private final ReturnCodesKeyRepository returnCodesKeyRepository;
	private final KeysManager keysManager;
	private final HashService hashService;
	private final CombinedCorrectnessInformationRepository combinedCorrectnessInformationRepository;
	private final CreateLCCShareService createLCCShareService;
	private final CreateLVCCShareService createLVCCShareService;

	private final ObjectMapper mapper;
	private final CryptolibPayloadSignatureService payloadSignatureService;
	private final String controlComponentId;
	private final String computationOutputQueue;

	private final ReturnCodesMessageFactory returnCodesMessageFactory;

	public ReturnCodesExponentiationConsumer(final RabbitTemplate rabbitTemplate,
			final ReturnCodesKeyRepository returnCodesKeyRepository,
			final KeysManager keysManager, final HashService hashService,
			final CombinedCorrectnessInformationRepository combinedCorrectnessInformationRepository,
			final CreateLCCShareService createLCCShareService,
			final CreateLVCCShareService createLVCCShareService, final ObjectMapper mapper,
			final CryptolibPayloadSignatureService payloadSignatureService,
			final ReturnCodesMessageFactory returnCodesMessageFactory,
			@Value("${keys.nodeId:defCcxId}")
			final String controlComponentId,
			@Value("${verification.computation.response.queue}")
			final String computationOutputQueue) {

		this.rabbitTemplate = rabbitTemplate;
		this.returnCodesKeyRepository = returnCodesKeyRepository;
		this.keysManager = keysManager;
		this.hashService = hashService;
		this.combinedCorrectnessInformationRepository = combinedCorrectnessInformationRepository;
		this.createLCCShareService = createLCCShareService;
		this.createLVCCShareService = createLVCCShareService;
		this.mapper = mapper;
		this.payloadSignatureService = payloadSignatureService;
		this.controlComponentId = controlComponentId;
		this.computationOutputQueue = computationOutputQueue;
		this.returnCodesMessageFactory = returnCodesMessageFactory;
	}

	@RabbitListener(queues = "${verification.computation.request.queue}", autoStartup = "false")
	public void onMessage(final Message message) throws SafeStreamDeserializationException {
		final StreamSerializableObjectReader<ReturnCodeComputationDTO<ReturnCodesInput>> reader = new StreamSerializableObjectReaderImpl<>();
		final ReturnCodeComputationDTO<ReturnCodesInput> data = reader.read(message.getBody(), 1, message.getBody().length);

		validateParameters(data);

		final LongReturnCodesSharePayload longReturnCodesSharePayload;
		try {
			longReturnCodesSharePayload = compute(data);
		} catch (ComputedVerificationCardNotFoundException | KeyManagementException e) {
			LOGGER.error("Failed to handle Return Codes Exponentiation request.", e);
			return;
		}

		sendResponse(data, longReturnCodesSharePayload);
	}

	private LongReturnCodesSharePayload compute(final ReturnCodeComputationDTO<ReturnCodesInput> data)
			throws ComputedVerificationCardNotFoundException, KeyManagementException {

		final ElGamalPrivateKey ccrjReturnCodesGenerationSecretKey = getCcrjReturnCodesGenerationSecretKey(data);
		final ZqElement secretKey = CryptoAdapters.convert(ccrjReturnCodesGenerationSecretKey.getKeys().get(0));
		final ZpSubgroup zpSubgroup = ccrjReturnCodesGenerationSecretKey.getGroup();
		final GqGroup gqGroup = new GqGroup(zpSubgroup.getP(), zpSubgroup.getQ(), zpSubgroup.getG());

		final ControlComponentContext context = new ControlComponentContext(data.getElectionEventId(), data.getVerificationCardSetId(),
				controlComponentId);

		final LongReturnCodesShare longReturnCodesShare;
		if (isChoiceReturnCodesComputation(data)) {
			// Long Choice Return Codes share computation.
			checkArgument(validateLCCShareInput(data));

			final List<BigInteger> partialChoiceReturnCodesValues = data.getPayload().getReturnCodesInputElements();
			final GroupVector<GqElement, GqGroup> partialChoiceReturnCodes = partialChoiceReturnCodesValues.stream()
					.map(bi -> GqElement.create(bi, gqGroup))
					.collect(GroupVector.toGroupVector());

			final CreateLCCShareOutput createLCCShareOutput = createLCCShareService
					.createLCCShare(partialChoiceReturnCodes, secretKey, data.getVerificationCardId(), context);

			// Create Secure log message.
			final LongChoiceReturnCodesShareExponentiationProof exponentiationProof = new LongChoiceReturnCodesShareExponentiationProof(
					data.getVerificationCardId(), createLCCShareOutput.getHashedPartialChoiceReturnCodes(),
					createLCCShareOutput.getLongChoiceReturnCodeShare(), createLCCShareOutput.getExponentiationProof());

			final ReturnCodesMessage message = returnCodesMessageFactory
					.buildLongChoiceReturnCodesShareExponentiationProofLogMessage(context, exponentiationProof);
			SECURE_LOGGER.info(message);

			// Create the lCC share.
			longReturnCodesShare = new LongChoiceReturnCodesShare(data.getCorrelationId(), data.getElectionEventId(), data.getVerificationCardSetId(),
					data.getVerificationCardId(), data.getRequestId(), createLCCShareOutput.getLongChoiceReturnCodeShare(),
					createLCCShareOutput.getVoterChoiceReturnCodeGenerationPublicKey(), createLCCShareOutput.getExponentiationProof());
		} else {
			// Long Vote Cast Return Code share computation.
			checkArgument(validateLVCCShareInput(data));

			final BigInteger confirmationKeyValue = data.getPayload().getReturnCodesInputElements().get(0);
			final GqElement confirmationKey = GqElement.create(confirmationKeyValue, gqGroup);

			final CreateLVCCShareOutput createLVCCShareOutput = createLVCCShareService
					.createLVCCShare(confirmationKey, secretKey, data.getVerificationCardId(), data.getElectionEventId());

			// Create Secure log message.
			final LongVoteCastReturnCodesShareExponentiationProof exponentiationProof = new LongVoteCastReturnCodesShareExponentiationProof(
					data.getVerificationCardId(), createLVCCShareOutput.getConfirmationAttempts(),
					createLVCCShareOutput.getHashedSquaredConfirmationKey(), createLVCCShareOutput.getLongVoteCastReturnCodeShare(),
					createLVCCShareOutput.getExponentiationProof());

			final ReturnCodesMessage message = returnCodesMessageFactory
					.buildLongVoteCastReturnCodesShareExponentiationProofLogMessage(context, exponentiationProof);
			SECURE_LOGGER.info(message);

			// Create the lVCC share.
			longReturnCodesShare = new LongVoteCastReturnCodesShare(data.getCorrelationId(), data.getElectionEventId(),
					data.getVerificationCardSetId(), data.getVerificationCardId(), data.getRequestId(),
					createLVCCShareOutput.getLongVoteCastReturnCodeShare(), createLVCCShareOutput.getVoterVoteCastReturnCodeGenerationPublicKey(),
					createLVCCShareOutput.getExponentiationProof());
		}

		return new LongReturnCodesSharePayload(gqGroup, longReturnCodesShare);
	}

	private void sendResponse(final ReturnCodeComputationDTO<ReturnCodesInput> data, final LongReturnCodesSharePayload longReturnCodesSharePayload) {
		final String electionEventId = data.getElectionEventId();
		final String verificationCardSetId = data.getVerificationCardSetId();
		final String verificationCardId = data.getVerificationCardId();

		// Sign response payload.
		final byte[] responsePayloadHash = hashService.recursiveHash(longReturnCodesSharePayload);
		final CryptoPrimitivesPayloadSignature payloadSignature;
		try {
			payloadSignature = payloadSignatureService.sign(responsePayloadHash, keysManager.getElectionSigningPrivateKey(electionEventId),
					keysManager.getElectionSigningCertificateChain(electionEventId));
		} catch (KeyManagementException | PayloadSignatureException e) {
			LOGGER.error(String.format("Failed to sign payload for electionEventId %s, verificationCardSetId %s and verificationCardId %s.",
					electionEventId, verificationCardSetId, verificationCardId), e);
			return;
		}
		longReturnCodesSharePayload.setSignature(payloadSignature);
		LOGGER.info(
				"Successfully signed the CCR_j long Return Codes shares payload for electionEventId {}, verificationCardSetId {} and verificationCardId {}",
				electionEventId, verificationCardSetId, verificationCardId);

		// Create response body.
		final byte[] body;
		try {
			body = mapper.writeValueAsBytes(longReturnCodesSharePayload);
		} catch (JsonProcessingException e) {
			LOGGER.error(String.format(
					"Failed to serialize CCR_j long Return Codes shares payload for electionEventId %s, verificationCardSetId %s and verificationCardId %s.",
					electionEventId, verificationCardSetId, verificationCardId), e);
			return;
		}

		// The MessagingService in the voting-server expects the first byte to be the type.
		byte[] byteContent = new byte[body.length + 1];
		byteContent[0] = 0;
		System.arraycopy(body, 0, byteContent, 1, body.length);

		rabbitTemplate.convertAndSend(computationOutputQueue, byteContent);
	}

	private void validateParameters(final ReturnCodeComputationDTO<ReturnCodesInput> data) {
		checkNotNull(data);
		checkArgument(!isBlank(data.getRequestId()), "Request id must not be empty.");
		validateUUID(data.getElectionEventId());
		validateUUID(data.getVerificationCardSetId());
		validateUUID(data.getVerificationCardId());
		checkNotNull(data.getPayload());
	}

	/**
	 * Checks the validity of the partial Choice Return Codes pCC.
	 *
	 * @param data The voting server's data object, containing the partial Choice Return Codes
	 */
	private boolean validateLCCShareInput(final ReturnCodeComputationDTO<ReturnCodesInput> data) {
		final List<BigInteger> partialChoiceReturnCodes = data.getPayload().getReturnCodesInputElements();

		if (partialChoiceReturnCodes == null) {
			LOGGER.error("{} the partial Choice Return Codes field in the payload was null",
					ReturnCodesVerificationServiceConstants.UNEXPECTED_SCENARIO_PROCESSING_CC);
			return false;
		}

		final CombinedCorrectnessInformation combinedCorrectnessInformation = getCombinedCorrectnessInformation(data.getElectionEventId(),
				data.getVerificationCardSetId());

		// Check that the number of partial choice return codes received matches the totalNumberOfSelections.
		if (partialChoiceReturnCodes.size() != combinedCorrectnessInformation.getTotalNumberOfSelections()) {
			LOGGER.error("The size of the partial choice return code list ({}) does not match the expected size ({}).",
					partialChoiceReturnCodes.size(), combinedCorrectnessInformation.getTotalNumberOfSelections());
			return false;
		}

		return true;
	}

	/**
	 * Checks the Confirmation Key CK.
	 *
	 * @param data The voting server's data object, containing the Confirmation Key.
	 */
	private boolean validateLVCCShareInput(final ReturnCodeComputationDTO<ReturnCodesInput> data) {
		try {
			final List<BigInteger> confirmationKey = data.getPayload().getReturnCodesInputElements();

			if (confirmationKey == null) {
				LOGGER.error("{} the confirmation key in the payload was null",
						ReturnCodesVerificationServiceConstants.UNEXPECTED_SCENARIO_PROCESSING_CC);
				return false;
			}

			if (confirmationKey.size() != 1) {
				LOGGER.error("Unexpected scenario: more than one confirmation key: {}", confirmationKey);
				return false;
			}

			final ObjectMapper objectMapper = DomainObjectMapper.getNewInstance();
			final TraceableConfirmationMessage votingClientConfirmationMessage = objectMapper
					.readValue(data.getPayload().getConfirmationKeyVerificationInput().getConfirmationMessage(), TraceableConfirmationMessage.class);
			final String votingClientConfirmationKeyString = new String(
					Base64.getDecoder().decode(votingClientConfirmationMessage.getConfirmationKey()), StandardCharsets.UTF_8);

			final BigInteger votingClientConfirmationKey = new BigInteger(votingClientConfirmationKeyString);
			final BigInteger votingServerConfirmationKey = confirmationKey.get(0);

			// Check that the voting client's confirmation key is not null.
			if (votingServerConfirmationKey == null) {
				LOGGER.error("Unexpected scenario when checking the confirmation key: the voting client's confirmation key in the payload is null");
				return false;
			}

			return votingServerConfirmationKey.equals(votingClientConfirmationKey);
		} catch (IOException e) {
			LOGGER.error("Unexpected error checking the group membership of the confirmation key and its consistency:", e);
			return false;
		}
	}

	private ElGamalPrivateKey getCcrjReturnCodesGenerationSecretKey(final ReturnCodeComputationDTO<ReturnCodesInput> data)
			throws KeyManagementException {

		final String electionEventId = data.getElectionEventId();
		final String verificationCardSetId = data.getVerificationCardSetId();

		return returnCodesKeyRepository.getCcrjReturnCodesGenerationSecretKey(electionEventId, verificationCardSetId);
	}

	private boolean isChoiceReturnCodesComputation(final ReturnCodeComputationDTO<ReturnCodesInput> data) {
		return data.getPayload().getConfirmationKeyVerificationInput() == null;
	}

	private CombinedCorrectnessInformation getCombinedCorrectnessInformation(final String electionEventId, final String verificationCardSetId) {
		return combinedCorrectnessInformationRepository.findByElectionEventIdAndVerificationCardSetId(electionEventId, verificationCardSetId);
	}

}
