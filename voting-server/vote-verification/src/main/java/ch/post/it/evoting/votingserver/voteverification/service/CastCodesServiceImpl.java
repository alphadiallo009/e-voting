/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.voteverification.service;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.cryptolib.certificates.utils.CryptographicOperationException;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.domain.election.model.confirmation.TraceableConfirmationMessage;
import ch.post.it.evoting.domain.returncodes.ConfirmationKeyVerificationInput;
import ch.post.it.evoting.domain.returncodes.LongVoteCastReturnCodesShare;
import ch.post.it.evoting.domain.returncodes.ReturnCodesInput;
import ch.post.it.evoting.domain.returncodes.ShortVoteCastReturnCodeAndComputeResults;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.voteverification.domain.model.verification.Verification;
import ch.post.it.evoting.votingserver.voteverification.domain.model.verification.VerificationRepository;

/**
 * Generate the short vote cast return code based on the confirmation message - in interaction with the control components.
 */
@Stateless
public class CastCodesServiceImpl implements CastCodeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CastCodesServiceImpl.class);

	@Inject
	private VerificationRepository verificationRepository;

	@Inject
	private CodesComputeService codesComputeService;

	@Inject
	private ExtractVCCService extractVCCService;

	@Inject
	private ObjectMapper mapper;

	@Override
	public ShortVoteCastReturnCodeAndComputeResults retrieveCastCode(final String tenantId, final String electionEventId,
			final String verificationCardId, final TraceableConfirmationMessage confirmationMessage)
			throws ResourceNotFoundException, CryptographicOperationException {

		LOGGER.info("Generating the vote cast code for tenant: {}, election event: {} and verification card: {}.", tenantId, electionEventId,
				verificationCardId);

		final Verification verification = verificationRepository
				.findByTenantIdElectionEventIdVerificationCardId(tenantId, electionEventId, verificationCardId);
		final String verificationCardSetId = verification.getVerificationCardSetId();

		final String confirmationCodeString = new String(Base64.getDecoder().decode(confirmationMessage.getConfirmationKey()),
				StandardCharsets.UTF_8);
		final BigInteger confirmationCodeValue = new BigInteger(confirmationCodeString);

		try {
			// Ask the control components to compute the long vote cast return code shares lCC_j_id.
			final List<LongVoteCastReturnCodesShare> computationResults = codesComputeService
					.computeLongVoteCastReturnCodeShares(tenantId, electionEventId, verificationCardSetId, verificationCardId,
							createVerificationPayload(Collections.singletonList(confirmationCodeValue), confirmationMessage));
			LOGGER.info("Successfully retrieved the long vote cast return code shares for card {}-{}-{}.", electionEventId,
					verificationCardSetId, verificationCardId);

			// Retrieve short codes by combining CCR shares and looking up the CMTable.
			final List<GqElement> lVCCShares = computationResults.stream()
					.map(LongVoteCastReturnCodesShare::getLongVoteCastReturnCodeShare)
					.collect(Collectors.toList());
			final String voteCastReturnCode = extractVCCService.extractVCC(lVCCShares, verificationCardId, electionEventId, tenantId);

			// Prepare response.
			final ShortVoteCastReturnCodeAndComputeResults castCodeMessage = new ShortVoteCastReturnCodeAndComputeResults();
			castCodeMessage.setShortVoteCastReturnCode(voteCastReturnCode);
			castCodeMessage.setComputationResults(mapper.writeValueAsString(computationResults));

			return castCodeMessage;
		} catch (JsonProcessingException e) {
			throw new CryptographicOperationException("Error retrieving the cast code message:", e);
		}
	}

	private ReturnCodesInput createVerificationPayload(final List<BigInteger> confirmationCodeToCompute,
			final TraceableConfirmationMessage confirmationMessage) throws JsonProcessingException {

		final ConfirmationKeyVerificationInput confirmationKeyVerificationInput = new ConfirmationKeyVerificationInput();
		confirmationKeyVerificationInput.setConfirmationMessage(mapper.writeValueAsString(confirmationMessage));
		confirmationKeyVerificationInput.setVotingCardId(confirmationMessage.getVotingCardId());

		final ReturnCodesInput returnCodesInput = new ReturnCodesInput();
		returnCodesInput.setReturnCodesInputElements(confirmationCodeToCompute);
		returnCodesInput.setConfirmationKeyVerificationInput(confirmationKeyVerificationInput);

		return returnCodesInput;
	}

}
