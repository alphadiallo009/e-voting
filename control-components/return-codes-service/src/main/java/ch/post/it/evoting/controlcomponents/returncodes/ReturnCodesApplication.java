/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import ch.post.it.evoting.controlcomponents.commons.CommonsConfig;

/**
 * The control components Return Codes (CCR) application
 */
@SpringBootApplication
@Import(CommonsConfig.class)
public class ReturnCodesApplication {

	public static void main(String[] args) {
		Security.addProvider(new BouncyCastleProvider());
		SpringApplication.run(ReturnCodesApplication.class);
	}
}


