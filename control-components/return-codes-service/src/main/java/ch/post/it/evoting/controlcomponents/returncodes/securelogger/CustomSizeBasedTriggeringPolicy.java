package ch.post.it.evoting.controlcomponents.returncodes.securelogger;

import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.rolling.RollingFileManager;
import org.apache.logging.log4j.core.appender.rolling.SizeBasedTriggeringPolicy;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import ch.post.it.evoting.controlcomponents.returncodes.securelogger.events.CheckpointSecureLogEvent;

@Plugin(name = "CustomSizeBasedTriggeringPolicy", category = Core.CATEGORY_NAME, printObject = true)
public class CustomSizeBasedTriggeringPolicy extends SizeBasedTriggeringPolicy {

	private final SizeBasedTriggeringPolicy delegate;

	public CustomSizeBasedTriggeringPolicy(String maxFileSize) {
		this.delegate = SizeBasedTriggeringPolicy.createPolicy(maxFileSize);
	}

	@Override
	public boolean isTriggeringEvent(LogEvent event) {
		return !(event instanceof CheckpointSecureLogEvent) && delegate.isTriggeringEvent(event);
	}

	@Override
	public long getMaxFileSize() {
		return delegate.getMaxFileSize();
	}

	@Override
	public void initialize(RollingFileManager aManager) {
		delegate.initialize(aManager);
	}

	@Override
	public String toString() {
		return delegate.toString();
	}

	/**
	 * Create a {@link CustomSizeBasedTriggeringPolicy}.
	 *
	 * @param size The size of the file before rollover is required.
	 * @return A {@link CustomSizeBasedTriggeringPolicy}.
	 */
	@PluginFactory
	public static CustomSizeBasedTriggeringPolicy createPolicy(
			@PluginAttribute("size")
			final String size) {
		return new CustomSizeBasedTriggeringPolicy(size);
	}

}
