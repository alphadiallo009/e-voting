/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain;

import static ch.post.it.evoting.cryptoprimitives.domain.validations.UUIDValidations.validateUUID;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.VerificationCardPublicKeyRepositoryException;
import ch.post.it.evoting.controlcomponents.returncodes.service.ReturnCodesKeyRepository;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.ZpSubgroup;
import ch.post.it.evoting.cryptoprimitives.elgamal.ElGamalMultiRecipientPublicKey;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.domain.mixnet.exceptions.FailedValidationException;

@Repository
public class VerificationCardPublicKeyRepository {

	private static final String SAVE_VERIFICATION_CARD_PUBLIC_KEY_SQL =
			"INSERT INTO CC_VERIFICATION_CARD_PUBLIC_KEY (ELECTION_EVENT_ID, VERIFICATION_CARD_ID, VERIFICATION_CARD_SET_ID, VERIFICATION_CARD_PUBLIC_KEY) VALUES (?,?,?,?)";

	private static final String FIND_BY_VERIFICATION_CARD_ID =
			"SELECT VERIFICATION_CARD_PUBLIC_KEY FROM CC_VERIFICATION_CARD_PUBLIC_KEY WHERE VERIFICATION_CARD_ID=?";

	private final DataSource dataSource;
	private final ObjectMapper objectMapper;
	private final ReturnCodesKeyRepository returnCodesKeyRepository;

	@Autowired
	public VerificationCardPublicKeyRepository(final DataSource dataSource, final ObjectMapper objectMapper,
			final ReturnCodesKeyRepository returnCodesKeyRepository) {
		this.dataSource = dataSource;
		this.objectMapper = objectMapper;
		this.returnCodesKeyRepository = returnCodesKeyRepository;
	}

	/**
	 * Saves a verification card public key with the key verification card id.
	 *
	 * @param verificationCardPublicKey the POJO that contains the needed ids and the public key.
	 * @throws NullPointerException                         if any input parameter is null.
	 * @throws FailedValidationException                    if the verification card id is not a valid UUID.
	 * @throws VerificationCardPublicKeyRepositoryException if an error occurs while saving of the verification card public key.
	 */
	public void saveVerificationCardPublicKey(final VerificationCardPublicKey verificationCardPublicKey) {
		try (final Connection connection = dataSource.getConnection();
				final PreparedStatement statement = connection.prepareStatement(SAVE_VERIFICATION_CARD_PUBLIC_KEY_SQL)) {
			statement.setString(1, verificationCardPublicKey.getElectionEventId());
			statement.setString(2, verificationCardPublicKey.getVerificationCardId());
			statement.setString(3, verificationCardPublicKey.getVerificationCardSetId());
			statement.setBytes(4, objectMapper.writeValueAsString(verificationCardPublicKey.getVerificationCardPublicKey())
					.getBytes(StandardCharsets.UTF_8));
			statement.executeUpdate();
		} catch (SQLIntegrityConstraintViolationException e) {
			throw new VerificationCardPublicKeyRepositoryException(
					String.format("Already existing verification card public key entry with verification card id %s.",
							verificationCardPublicKey.getVerificationCardId()), e);
		} catch (SQLException | JsonProcessingException e) {
			throw new VerificationCardPublicKeyRepositoryException(
					String.format("An error occurred while saving the provided verification card public key entry with verification card id %s.",
							verificationCardPublicKey.getVerificationCardId()), e);
		}
	}

	/**
	 * Retrieves a verification card public key by the provided verification card id.
	 *
	 * @param electionEventId       the election event id.
	 * @param verificationCardSetId the verification card set id.
	 * @param verificationCardId    the verification card id.
	 * @throws NullPointerException                         if the the verification card id parameter is null.
	 * @throws FailedValidationException                    if the verification card id is not a valid UUID.
	 * @throws VerificationCardPublicKeyRepositoryException if an error occurs while retrieving the desired verification card public key.
	 */
	public ElGamalMultiRecipientPublicKey findByIds(final String electionEventId,
			final String verificationCardSetId, final String verificationCardId) {

		validateUUID(verificationCardId);

		try (final Connection connection = dataSource.getConnection();
				final PreparedStatement statement = connection.prepareStatement(FIND_BY_VERIFICATION_CARD_ID)) {
			statement.setString(1, verificationCardId);

			try (final ResultSet resultSet = statement.executeQuery()) {
				if (resultSet.next()) {

					final ZpSubgroup mathematicalGroup = returnCodesKeyRepository.getMathematicalGroup(electionEventId, verificationCardSetId);
					final GqGroup gqGroup = new GqGroup(mathematicalGroup.getP(), mathematicalGroup.getQ(), mathematicalGroup.getG());

					return objectMapper.reader().withAttribute("group", gqGroup)
							.readValue(resultSet.getBytes(1), ElGamalMultiRecipientPublicKey.class);
				} else {
					throw new VerificationCardPublicKeyRepositoryException(
							String.format("No verification card public key entry found for verification card id %s.", verificationCardId));
				}
			}
		} catch (SQLException | IOException | KeyManagementException e) {
			throw new VerificationCardPublicKeyRepositoryException(
					String.format("An error occurred while retrieving the verification card public key entry for verification card id %s.",
							verificationCardId), e);
		}
	}
}