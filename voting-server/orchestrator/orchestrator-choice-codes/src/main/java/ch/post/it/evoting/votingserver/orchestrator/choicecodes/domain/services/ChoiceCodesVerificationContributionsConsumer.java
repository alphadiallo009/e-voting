/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.choicecodes.domain.services;

import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.post.it.evoting.domain.returncodes.LongReturnCodesSharePayload;
import ch.post.it.evoting.votingserver.commons.messaging.MessageListener;
import ch.post.it.evoting.votingserver.commons.messaging.MessagingException;
import ch.post.it.evoting.votingserver.commons.messaging.MessagingService;
import ch.post.it.evoting.votingserver.commons.messaging.Queue;
import ch.post.it.evoting.votingserver.orchestrator.commons.config.QueuesConfig;
import ch.post.it.evoting.votingserver.orchestrator.commons.config.TopicsConfig;
import ch.post.it.evoting.votingserver.orchestrator.commons.polling.ReactiveResultsHandler;

@Contributions
@ChoiceCodesVerification
public class ChoiceCodesVerificationContributionsConsumer implements MessageListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceCodesVerificationContributionsConsumer.class);

	@Inject
	@ResultsReady
	@ChoiceCodesVerification
	private MessageListener resultsReadyConsumer;

	@Inject
	private MessagingService messagingService;

	@Inject
	private ChoiceCodesVerificationContributionsService ccvContributionsService;

	@Inject
	@ChoiceCodesVerification
	private ReactiveResultsHandler<List<byte[]>> verificationResultsHandler;

	@Override
	public void onMessage(final Object message) {
		final byte[] messageBytes = (byte[]) message;

		final LongReturnCodesSharePayload longReturnCodesSharePayload = ccvContributionsService.deserializeContribution(messageBytes);

		final UUID correlationId = longReturnCodesSharePayload.getLongReturnCodesShare().getCorrelationId();
		LOGGER.info("OR - Message with correlation identifier {} is accepted in {}", correlationId, getClass().getSimpleName());

		ccvContributionsService.persistPartialResult(correlationId, longReturnCodesSharePayload);
		LOGGER.info("OR - Partial result with identifier {} persisted in {}.", correlationId, getClass().getSimpleName());

		// If all contributions are ready, notify it.
		if (ccvContributionsService.hasAllPartialResults(correlationId, partialResultCount())) {
			LOGGER.info("OR - All partial results with correlation identifier {} are ready in {}", correlationId, getClass().getSimpleName());

			verificationResultsHandler.resultsReady(correlationId);
			LOGGER.info("OR - Correlation identifier {} is sent to {}.", correlationId, TopicsConfig.HA_TOPIC);
		}
	}

	public void startup() throws MessagingException {
		consumeResultsReady();
		LOGGER.info("OR - Consuming the results ready notifications for choice codes verification");
		consumeComputeContributions();
		LOGGER.info("OR - Consuming the choice codes verification contributions");
	}

	public void shutdown() throws MessagingException {
		stopConsumingResultsReady();
		LOGGER.info("OR - Consuming the results ready notifications for choice codes verification is stopped");
		stopConsumingComputeContributions();
		LOGGER.info("OR - Consuming the choice codes verification contributions is stopped");
	}

	protected int partialResultCount() {
		return QueuesConfig.VERIFICATION_COMPUTE_CONTRIBUTIONS_RES_QUEUES.length;
	}

	private void consumeComputeContributions() throws MessagingException {
		final Queue[] queues = QueuesConfig.VERIFICATION_COMPUTE_CONTRIBUTIONS_RES_QUEUES;

		if (queues.length == 0) {
			throw new IllegalStateException("No choice codes compute contributions response queues provided to listen to.");
		}

		for (Queue queue : queues) {
			messagingService.createReceiver(queue, this);
		}
	}

	private void consumeResultsReady() throws MessagingException {
		messagingService.createReceiver(TopicsConfig.HA_TOPIC, resultsReadyConsumer);
	}

	private void stopConsumingComputeContributions() throws MessagingException {
		final Queue[] queues = QueuesConfig.VERIFICATION_COMPUTE_CONTRIBUTIONS_RES_QUEUES;

		if (queues.length == 0) {
			throw new IllegalStateException("No choice codes compute contributions response queues provided to listen to.");
		}

		for (Queue queue : queues) {
			messagingService.destroyReceiver(queue, this);
		}
	}

	private void stopConsumingResultsReady() throws MessagingException {
		messagingService.destroyReceiver(TopicsConfig.HA_TOPIC, resultsReadyConsumer);
	}

}


