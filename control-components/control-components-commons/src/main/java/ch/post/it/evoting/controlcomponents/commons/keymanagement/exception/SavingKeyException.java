/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.commons.keymanagement.exception;

import java.security.KeyManagementException;

public final class SavingKeyException extends KeyManagementException {

	private static final long serialVersionUID = 1L;

	public SavingKeyException(String message, Throwable cause) {
		super(message, cause);
	}
}