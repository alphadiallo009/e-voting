/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.domain;

import ch.post.it.evoting.cryptoprimitives.GroupVector;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;

/**
 * Encapsulates an exponentiation proof for the exponentiation of the hashed and squared partial choice return codes with the voter choice return code
 * generation secret key. This object also includes the bases and exponentiated elements. However to avoid redundancy, object does not contain the
 * public key and the context information that is necessary to verify the proofs.
 */
public class LongChoiceReturnCodesShareExponentiationProof {

	private final String verificationCardId;
	private final GroupVector<GqElement, GqGroup> hashedPartialChoiceReturnCodes;
	private final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare;
	private final ExponentiationProof exponentiationProof;

	public LongChoiceReturnCodesShareExponentiationProof(final String verificationCardId,
			final GroupVector<GqElement, GqGroup> hashedPartialChoiceReturnCodes, final GroupVector<GqElement, GqGroup> longChoiceReturnCodeShare,
			final ExponentiationProof exponentiationProof) {

		this.verificationCardId = verificationCardId;
		this.hashedPartialChoiceReturnCodes = hashedPartialChoiceReturnCodes;
		this.longChoiceReturnCodeShare = longChoiceReturnCodeShare;
		this.exponentiationProof = exponentiationProof;
	}

	public String getVerificationCardId() {
		return verificationCardId;
	}

	public GroupVector<GqElement, GqGroup> getHashedPartialChoiceReturnCodes() {
		return hashedPartialChoiceReturnCodes;
	}

	public GroupVector<GqElement, GqGroup> getLongChoiceReturnCodeShare() {
		return longChoiceReturnCodeShare;
	}

	public ExponentiationProof getExponentiationProof() {
		return exponentiationProof;
	}
}
