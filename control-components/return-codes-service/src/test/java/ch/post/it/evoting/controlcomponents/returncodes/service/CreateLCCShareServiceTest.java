/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.service;

import static ch.post.it.evoting.controlcomponents.returncodes.service.CreateLCCShareService.CreateLCCShareOutput;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import ch.post.it.evoting.controlcomponents.returncodes.domain.CombinedCorrectnessInformationRepository;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ComputedVerificationCard;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ComputedVerificationCardsRepository;
import ch.post.it.evoting.controlcomponents.returncodes.domain.ControlComponentContext;
import ch.post.it.evoting.controlcomponents.returncodes.domain.exception.ComputedVerificationCardNotFoundException;
import ch.post.it.evoting.cryptolib.api.exceptions.GeneralCryptoLibException;
import ch.post.it.evoting.cryptolib.mathematical.groups.impl.Exponent;
import ch.post.it.evoting.cryptoprimitives.CryptoPrimitivesService;
import ch.post.it.evoting.cryptoprimitives.GroupVector;
import ch.post.it.evoting.cryptoprimitives.TestGroupSetup;
import ch.post.it.evoting.cryptoprimitives.domain.election.CombinedCorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.domain.election.CorrectnessInformation;
import ch.post.it.evoting.cryptoprimitives.hashing.HashService;
import ch.post.it.evoting.cryptoprimitives.math.GqElement;
import ch.post.it.evoting.cryptoprimitives.math.GqGroup;
import ch.post.it.evoting.cryptoprimitives.math.ZqElement;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ExponentiationProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProof;
import ch.post.it.evoting.cryptoprimitives.zeroknowledgeproofs.ZeroKnowledgeProofService;
import ch.post.it.evoting.domain.cryptoadapters.CryptoAdapters;

@DisplayName("CreateLCCShareService")
class CreateLCCShareServiceTest extends TestGroupSetup {

	private static final int NODE_ID = 1;
	private static final int PSI = 5;
	private static final int l_ID = 32;
	private static final String CORRECTNESS_ID = "1";
	private static final HashService hashService = spy(HashService.class);
	private static final ZeroKnowledgeProof zeroKnowledgeProofService = spy(ZeroKnowledgeProofService.class);
	private static final ComputedVerificationCardsRepository computedVerificationCardRepositoryMock = mock(ComputedVerificationCardsRepository.class);
	private static final CombinedCorrectnessInformationRepository combinedCorrectnessInformationRepositoryMock = mock(
			CombinedCorrectnessInformationRepository.class);
	private static final VoterReturnCodeGenerationKeyDerivationService voterReturnCodeGenerationKeyDerivationServiceMock = mock(
			VoterReturnCodeGenerationKeyDerivationService.class);

	private static CreateLCCShareService createLCCShareService;

	@BeforeAll
	static void setUpAll() {
		createLCCShareService = new CreateLCCShareService(NODE_ID, hashService, zeroKnowledgeProofService, computedVerificationCardRepositoryMock,
				combinedCorrectnessInformationRepositoryMock, voterReturnCodeGenerationKeyDerivationServiceMock);
	}

	@Nested
	@DisplayName("calling createLCCShare with")
	class createLCCShare {

		private GroupVector<GqElement, GqGroup> partialChoiceReturnCodes;
		private ZqElement ccrjReturnCodesGenerationSecretKey;
		private String verificationCardId;
		private ControlComponentContext context;

		@BeforeEach
		void setUp() {
			boolean allDistinct;
			do {
				partialChoiceReturnCodes = gqGroupGenerator.genRandomGqElementVector(PSI);
				allDistinct = partialChoiceReturnCodes.stream()
						.allMatch(ConcurrentHashMap.newKeySet()::add);
			}
			while (!allDistinct);

			ccrjReturnCodesGenerationSecretKey = zqGroupGenerator.genRandomZqElementMember();
			verificationCardId = CryptoPrimitivesService.get().genRandomBase16String(l_ID).toLowerCase();

			final String electionEventId = CryptoPrimitivesService.get().genRandomBase16String(l_ID).toLowerCase();
			final String verificationCardSetId = CryptoPrimitivesService.get().genRandomBase16String(l_ID).toLowerCase();
			context = new ControlComponentContext(electionEventId, verificationCardSetId, String.valueOf(NODE_ID));

			reset(computedVerificationCardRepositoryMock);
			reset(combinedCorrectnessInformationRepositoryMock);
			reset(voterReturnCodeGenerationKeyDerivationServiceMock);
		}

		@Test
		@DisplayName("valid parameters does not throw")
		void validParameters() throws GeneralCryptoLibException, ComputedVerificationCardNotFoundException {
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation(CORRECTNESS_ID, PSI, PSI)));
			when(combinedCorrectnessInformationRepositoryMock.findByElectionEventIdAndVerificationCardSetId(context.getElectionEventId(),
					context.getVerificationCardSetId())).thenReturn(combinedCorrectnessInformation);

			final ComputedVerificationCard computedVerificationCard = new ComputedVerificationCard(context.getElectionEventId(), verificationCardId);
			when(computedVerificationCardRepositoryMock.findByElectionEventIdAndVerificationCardId(context.getElectionEventId(),
					verificationCardId)).thenReturn(Optional.of(computedVerificationCard));

			final Exponent exponent = CryptoAdapters.convert(ccrjReturnCodesGenerationSecretKey);
			final Exponent derivedKey = CryptoAdapters.convert(zqGroupGenerator.genRandomZqElementMember());
			when(voterReturnCodeGenerationKeyDerivationServiceMock
					.deriveVoterReturnCodeGenerationPrivateKey(exponent, verificationCardId, gqGroup.getQ())).thenReturn(derivedKey);

			doReturn(0).when(hashService).getHashLength();
			doReturn(new byte[] { 0x4 }).when(hashService).recursiveHash(any());

			final ExponentiationProof exponentiationProof = new ExponentiationProof(zqGroupGenerator.genRandomZqElementMember(),
					zqGroupGenerator.genRandomZqElementMember());
			doReturn(exponentiationProof).when(zeroKnowledgeProofService).genExponentiationProof(any(), any(), any(), any());

			doNothing().when(computedVerificationCardRepositoryMock).updateComputedVerificationCard(computedVerificationCard);

			final CreateLCCShareOutput output = createLCCShareService
					.createLCCShare(partialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId, context);

			assertEquals(PSI, output.getHashedPartialChoiceReturnCodes().size());
			assertEquals(PSI, output.getLongChoiceReturnCodeShare().size());
			assertTrue(gqGroup.hasSameOrderAs(output.getExponentiationProof().getGroup()));
			assertTrue(gqGroup.hasSameOrderAs(output.getVoterChoiceReturnCodeGenerationPublicKey().getGroup()));
		}

		@Test
		@DisplayName("any null parameter throws NullPointerException")
		void nullParameters() {
			assertAll(
					() -> assertThrows(NullPointerException.class, () -> createLCCShareService
							.createLCCShare(null, ccrjReturnCodesGenerationSecretKey, verificationCardId, context)),
					() -> assertThrows(NullPointerException.class,
							() -> createLCCShareService.createLCCShare(partialChoiceReturnCodes, null, verificationCardId, context)),
					() -> assertThrows(NullPointerException.class, () -> createLCCShareService
							.createLCCShare(partialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey, null, context)),
					() -> assertThrows(NullPointerException.class, () -> createLCCShareService
							.createLCCShare(partialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId, null))
			);
		}

		@Test
		@DisplayName("codes and keys having different group order throws IllegalArgumentException")
		void diffGroupCodesKeys() {
			final ZqElement otherGroupSecretKey = otherZqGroupGenerator.genRandomZqElementMember();

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
					() -> createLCCShareService.createLCCShare(partialChoiceReturnCodes, otherGroupSecretKey, verificationCardId, context));
			assertEquals("The partial choice return codes and return codes generation secret key must have the same group order.",
					exception.getMessage());
		}

		@Test
		@DisplayName("partial codes not all distinct throws IllegalArgumentException")
		void notDistinctCodes() {
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation(CORRECTNESS_ID, PSI, PSI)));
			when(combinedCorrectnessInformationRepositoryMock.findByElectionEventIdAndVerificationCardSetId(context.getElectionEventId(),
					context.getVerificationCardSetId())).thenReturn(combinedCorrectnessInformation);

			final GroupVector<GqElement, GqGroup> notDistinctCodes = partialChoiceReturnCodes.append(partialChoiceReturnCodes.get(0));

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> createLCCShareService
					.createLCCShare(notDistinctCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId, context));
			assertEquals(String.format("The number of partial choice return codes (%s) must be equal to psi (%s).", PSI + 1, PSI),
					exception.getMessage());
		}

		@Test
		@DisplayName("partial codes not previously decrypted throws IllegalArgumentException")
		void notPreviouslyDecryptedCodes() {
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation(CORRECTNESS_ID, PSI, PSI)));
			when(combinedCorrectnessInformationRepositoryMock.findByElectionEventIdAndVerificationCardSetId(context.getElectionEventId(),
					context.getVerificationCardSetId())).thenReturn(combinedCorrectnessInformation);

			when(computedVerificationCardRepositoryMock.findByElectionEventIdAndVerificationCardId(context.getElectionEventId(),
					verificationCardId)).thenReturn(Optional.empty());

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> createLCCShareService
					.createLCCShare(partialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId, context));
			assertEquals(String.format("The CCR_j did not decrypt the partial Choice Return Codes for verification card %s.", verificationCardId),
					exception.getMessage());
		}

		@Test
		@DisplayName("long choice return codes share already generated throws IllegalArgumentException")
		void alreadyGeneratedLongChoiceReturnCodesShare() {
			final CombinedCorrectnessInformation combinedCorrectnessInformation = new CombinedCorrectnessInformation(
					Collections.singletonList(new CorrectnessInformation("1", PSI, PSI)));
			when(combinedCorrectnessInformationRepositoryMock.findByElectionEventIdAndVerificationCardSetId(context.getElectionEventId(),
					context.getVerificationCardSetId())).thenReturn(combinedCorrectnessInformation);

			final ComputedVerificationCard verificationCard = new ComputedVerificationCard(context.getElectionEventId(), verificationCardId);
			verificationCard.setExponentiationComputed(true);
			when(computedVerificationCardRepositoryMock.findByElectionEventIdAndVerificationCardId(context.getElectionEventId(),
					verificationCardId)).thenReturn(Optional.of(verificationCard));

			final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> createLCCShareService
					.createLCCShare(partialChoiceReturnCodes, ccrjReturnCodesGenerationSecretKey, verificationCardId, context));
			assertEquals(
					String.format("The CCR_j already generated the long Choice Return Code share in a previous attempt for verification card %s.",
							verificationCardId), exception.getMessage());
		}

	}

}