/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.controlcomponents.returncodes.service.exception;

public class KeyDerivationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public KeyDerivationException(final String verificationCardId) {
		super(String.format("The derivation of the private key has failed for verificationCardId %s", verificationCardId));
	}

}
