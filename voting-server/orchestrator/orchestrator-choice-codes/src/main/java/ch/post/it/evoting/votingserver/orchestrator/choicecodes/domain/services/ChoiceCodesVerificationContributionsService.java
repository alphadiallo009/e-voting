/*
 * (c) Copyright 2021 Swiss Post Ltd.
 */
package ch.post.it.evoting.votingserver.orchestrator.choicecodes.domain.services;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.post.it.evoting.domain.returncodes.LongReturnCodesSharePayload;
import ch.post.it.evoting.domain.returncodes.ReturnCodeComputationDTO;
import ch.post.it.evoting.domain.returncodes.ReturnCodesInput;
import ch.post.it.evoting.votingserver.commons.beans.exceptions.ResourceNotFoundException;
import ch.post.it.evoting.votingserver.commons.messaging.MessagingException;
import ch.post.it.evoting.votingserver.commons.messaging.MessagingService;
import ch.post.it.evoting.votingserver.commons.messaging.Queue;
import ch.post.it.evoting.votingserver.orchestrator.commons.config.QueuesConfig;
import ch.post.it.evoting.votingserver.orchestrator.commons.infrastructure.persistence.Jdbc;
import ch.post.it.evoting.votingserver.orchestrator.commons.infrastructure.persistence.PartialResultsRepository;
import ch.post.it.evoting.votingserver.orchestrator.commons.polling.PollingService;

public class ChoiceCodesVerificationContributionsService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ChoiceCodesVerificationContributionsService.class);

	@Inject
	private MessagingService messagingService;

	@Jdbc
	@Inject
	private PartialResultsRepository<byte[]> repository;

	@Inject
	@ChoiceCodesVerification
	private PollingService<List<byte[]>> pollingService;

	@Inject
	private ObjectMapper mapper;

	public List<LongReturnCodesSharePayload> request(final String trackingId, final String electionEventId,
			final String verificationCardSetId, final String verificationCardId, final ReturnCodesInput partialCodes)
			throws ResourceNotFoundException {

		LOGGER.info("OR - Requesting the choice codes compute contributions for the voting phase.");

		final ReturnCodeComputationDTO<ReturnCodesInput> requestDTO = publishPartialCodesToReqQ(electionEventId, verificationCardSetId,
				verificationCardId, trackingId, partialCodes);

		return collectPartialCodesContributionsResponses(requestDTO.getCorrelationId());
	}

	public void persistPartialResult(final UUID correlationId, final LongReturnCodesSharePayload result) {
		byte[] bytes;
		try {
			bytes = mapper.writeValueAsBytes(result);
		} catch (IOException e) {
			throw new UncheckedIOException(String.format("Failed to serialize partial result for correlation identifier %s", correlationId), e);
		}
		repository.save(correlationId, bytes);
	}

	public boolean hasAllPartialResults(final UUID correlationId, final int partialResultCount) {
		return repository.hasAll(correlationId, partialResultCount);
	}

	public LongReturnCodesSharePayload deserializeContribution(final byte[] bytes) {
		try {
			return mapper.readValue(bytes, LongReturnCodesSharePayload.class);
		} catch (IOException e) {
			throw new UncheckedIOException("Failed to deserialize contribution into LongReturnCodesShare.", e);
		}
	}

	private ReturnCodeComputationDTO<ReturnCodesInput> publishPartialCodesToReqQ(final String electionEventId, final String verificationCardSetId,
			final String verificationCardId, final String trackingId, final ReturnCodesInput partialCodes) throws ResourceNotFoundException {

		final Queue[] queues = QueuesConfig.VERIFICATION_COMPUTE_CONTRIBUTIONS_REQ_QUEUES;

		if (queues.length == 0) {
			throw new IllegalStateException("No choice codes compute contributions request queues provided to publish to.");
		}

		final UUID correlationId = UUID.randomUUID();

		final ReturnCodeComputationDTO<ReturnCodesInput> returnCodeComputationDTO = new ReturnCodeComputationDTO<>(correlationId, trackingId,
				electionEventId, verificationCardSetId, verificationCardId, partialCodes);

		for (final Queue queue : queues) {
			try {
				messagingService.send(queue, returnCodeComputationDTO);
			} catch (MessagingException e) {
				throw new ResourceNotFoundException("Error publishing partial codes to the nodes queues ", e);
			}
		}

		return returnCodeComputationDTO;
	}

	private List<LongReturnCodesSharePayload> collectPartialCodesContributionsResponses(final UUID correlationId)
			throws ResourceNotFoundException {

		final List<byte[]> collectedContributions;
		try {
			LOGGER.info("OR - Waiting for the compute contributions from the nodes to be returned.");
			collectedContributions = pollingService.getResults(correlationId);
		} catch (TimeoutException e) {
			throw new ResourceNotFoundException("Error collecting partial codes from the nodes queues ", e);
		}

		return collectedContributions.stream()
				.map(this::deserializeContribution)
				.collect(Collectors.toList());
	}

}
